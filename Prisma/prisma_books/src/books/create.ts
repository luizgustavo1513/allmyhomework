import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient()

async function main() {
    const book = await prisma.books.create({
        data: {
            name: "Lampejos de um front brilhante",
            authorsId: "d49a33ad-41a4-41ea-97d6-8c9de3f0b7e5"
        }
    })
    console.log(book)
}

main()