import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient()

async function main() {
    const author = await prisma.authors.create({
        data: {
            name: "Miguel Dagostim Patrício",
        }
    })
    console.log(author)
}

main()