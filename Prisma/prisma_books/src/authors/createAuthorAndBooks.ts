import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient()

async function main() {
    const author = await prisma.authors.create({
        data: {
            name: "Carlos Vigarista",
            books: {
                createMany: {
                    data: [
                        {
                            name: "Como enganar tansão"
                        },
                        {
                            name: "Aonde fora meu papagaio?"
                        },
                        {
                            name: "Como saber ser bom. Um guia do cara que sabe ser bom"
                        },
                        {
                            name: "Backend nunca erra. Porque front é o culpado de tudo"
                        }
                    ]
                }
            }
        }
    })
    console.log(author)
}

main()