import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient()

async function main() {
    const result = await prisma.courses.create({
        data: {
            duration: 200,
            name: "curso de node",
            teacher: {
                create: {
                    name: "Daniel Lima"
                }
            }
        }
    })
    console.log(result)
}

main()