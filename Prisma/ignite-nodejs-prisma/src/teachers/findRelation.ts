import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient()

async function main() {
    const info = await prisma.courses.findMany({
        include: {
            teacher: true
        }
    })
    console.log(info)
}

main()