import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient()

async function main() {
    const filter = await prisma.courses.findMany({
        where: {
            OR: [
                {
                    name: {
                        startsWith: "Contar"
                    }
                },
                {
                    name: {
                        contains: "Guitarra",
                        mode: "insensitive"
                    }
                }
            ],
            AND: {
                duration: 300
            }
        }
    })
    console.log(filter)
}

main()