import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient()

async function main() {
    const filter = await prisma.courses.findMany({
        where: {
            name: {
                startsWith: "curso", // n ignora caixa alta
                mode: "insensitive" // ignora caixa alta
            }
        }
    })
    console.log(filter)
}

main()