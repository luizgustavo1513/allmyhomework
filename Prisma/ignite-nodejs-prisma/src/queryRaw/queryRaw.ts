import { PrismaClient, Prisma, Modules } from "@prisma/client";

const prisma = new PrismaClient()

async function main() {
    const filter = await prisma.$queryRaw<Modules[]>(
        Prisma.sql`SELECT *  FROM modules`
    )

    filter.map((item) => { console.log(item.name) })
    console.log(filter)
}

main()