import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient()

async function main() {
    const find = await prisma.courses.findMany({

        include: {
            CoursesModules: true
        }
    })
    console.log(JSON.stringify(find))
}

main()