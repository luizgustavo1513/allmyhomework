import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient()

async function findOneCourse() {
    const firstCourse = await prisma.courses.findFirst()
    const lastCourse = await prisma.courses.findFirst({ take: 1 })
    console.log("Primeiro registro criado: ", firstCourse)
    console.log("Último registro criado: ", lastCourse)
}

findOneCourse()