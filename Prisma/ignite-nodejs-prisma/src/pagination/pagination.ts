import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient()

async function main() {

    let skip = 0
    let exist = true
    while (exist) {
        const pagination = await prisma.courses.findMany({
            skip: skip,
            take: 2
        })
        skip += 1
        console.log(pagination)
        console.log("--------------------------")
        if (pagination.length <= 0) {
            exist = false
        }
    }
}

main()