import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient()

async function main() {
    const module = await prisma.modules.create({
        data: {
            description: "Aprendendo Firebase do zero",
            name: "Aprendendo Firebase",
            CoursesModules: {
                create: {
                    courses: {
                        connect: {
                            id: "79ec6239-ccdb-4b98-a774-efff7c5a9be3"
                        }
                    }
                }
            }
        }
    })
}

main()