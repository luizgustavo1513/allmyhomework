import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient()

async function main() {
    const result = await prisma.coursesModules.create({
        data: {
            fk_id_Courses: "79ec6239-ccdb-4b98-a774-efff7c5a9be3",
            fk_id_modules: "c1322280-2d8b-4f4a-93d9-84046e524fde"
        }
    })
}

main()