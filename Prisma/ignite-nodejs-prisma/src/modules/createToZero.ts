import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient()

async function main() {
    const doZero = await prisma.coursesModules.create({
        data: {
            courses: {
                create: {
                    name: "Curso de guitarra",
                    duration: 300,
                    teacher: {
                        create: {
                            name: "Donavan"
                        }
                    }
                }
            },
            modules: {
                create: {
                    name: "Entendendo as notas"
                }
            }
        }
    })
    console.log(doZero)
}

main()