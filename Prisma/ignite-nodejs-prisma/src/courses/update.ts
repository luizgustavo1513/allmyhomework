import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

async function main() {
  const result = await prisma.courses.update({
    where: {
      id: "948150cc-9fbf-4ba7-89f9-935d8380cb80",
    },
    data: {
      duration: 250,
      name: "Curso de Prisma",
      description: "Módulo da RocketSeat"
    },
  });

  console.log(result);
}

main();
