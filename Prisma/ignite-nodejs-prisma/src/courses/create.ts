import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

async function main() {
  const result = await prisma.courses.create({
    data: {
      name: "Curso de React Prisma",
      duration: 300,
      description: "Curso excelente de Prisma com a Dani",
      teacher: {
        connectOrCreate: {
          where: {
            name: "Daniele Leão"
          },
          create: {
            name: "Daniele Leão"
          }
        }
      }
    },
  });

  console.log(result);
}

main();
