
import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

async function main() {
    const result = await prisma.courses.create({
        data: {
            name: "Curso de futebol",
            duration: 450,
            description: "Curso excelente de futebol com o mais viciado de todos",
            fk_id_teacher: "3bb57c64-b27b-403b-89db-a0ea52fb1e5d"

        },
    });

    console.log(result);
}

main();
