
import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

async function main() {
    const result = await prisma.courses.create({
        data: {
            name: "Curso de narração",
            duration: 450,
            description: "Curso excelente de narração com o melhor de todos",
            teacher: {
                connect: {
                    id: "71854d1d-f55f-42f1-9f31-7154c659301b"
                }
            }
        },
    });

    console.log(result);
}

main();
