
import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

async function main() {
    const result = await prisma.courses.create({
        data: {
            name: "Curso de literatura",
            duration: 450,
            description: "Curso excelente de literatura com o melhor de todos",
            teacher: {
                create: {
                    name: "Monteiro Lobato"
                }
            }
        },
    });

    console.log(result);
}

main();
