-- CreateTable
CREATE TABLE "course_modules" (
    "id" TEXT NOT NULL,
    "fk_id_Courses" TEXT NOT NULL,
    "fk_id_modules" TEXT NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "course_modules_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "course_modules" ADD CONSTRAINT "course_modules_fk_id_Courses_fkey" FOREIGN KEY ("fk_id_Courses") REFERENCES "courses"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "course_modules" ADD CONSTRAINT "course_modules_fk_id_modules_fkey" FOREIGN KEY ("fk_id_modules") REFERENCES "modules"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
