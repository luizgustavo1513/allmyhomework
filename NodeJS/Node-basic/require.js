//módulos nativos

const path = require('path') //path é um módulo que quando atribuido a uma variável por meio do require executa diversas funcionalidades

console.log(path.basename('/Users/darwin/docs.js'))

const MyModule = require('./exports')

console.log(MyModule)