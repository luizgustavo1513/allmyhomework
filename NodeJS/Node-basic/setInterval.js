//setInterval irá rodar uma função N vezes depois de x milisegundos

const timeOut = 3000
const checking = () => console.log('checking!')

setInterval(checking, timeOut)
