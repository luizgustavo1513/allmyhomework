//setTimeout rodar uma função depois de x milisegundos

const timeOut = 3000
const finished = () => console.log('done!')

setTimeout(finished,timeOut) //callback

console.log('mostrar') //exemplo de assincronia, ele não espera os 3 segundos de cima pra executar 