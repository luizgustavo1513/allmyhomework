const { EventEmitter } = require('events')
//console.log(EventEmitter)

const ev = new EventEmitter
//console.log(ev)

ev.on('say something', (message) => { //ouvinte de eventos / once executa uma única vez
    console.log('alalal', message) //executor de eventos
})

ev.emit('say something', 'cao') //emissor de eventos

ev.emit('say something', 'bruno')

ev.emit('say something', 'luiz')