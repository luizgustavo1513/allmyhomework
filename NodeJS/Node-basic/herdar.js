const { inherits } = require('util')
const { EventEmitter } = require('events')

function Character(name){
    this.name = name
}

inherits(Character, EventEmitter) //faz o primeiro herdar as funcionalidades do segundo

const chapolin = new Character('Chapolin')
chapolin.on('help', () => { console.log(`Eu! O ${chapolin.name} colorado!`)})

console.log('Quem é velho broxa?')
chapolin.emit('help')