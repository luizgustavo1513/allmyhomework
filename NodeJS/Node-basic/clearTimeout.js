//clearTimeout cancelar o setTimeout

const timeOut = 3000
const finished = () => console.log('done!')

let timer = setTimeout(finished,timeOut)
clearTimeout(timer)

console.log(timer) 