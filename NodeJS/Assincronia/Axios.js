const axios = require('axios')

axios.get("https://api.github.com/users/maykbrito")
.then(res => {const response = res.data
    
    axios.get(response.repos_url)
    .then(repos=>console.log(repos))
    .catch(console.log(error)) //muito desorganizado, um then dentro do outro

})
.catch(error => console.log(error))


axios.get("https://api.github.com/users/maykbrito")
.then(res => axios.get(res.data.repos_url)) //mais organizado, executa um then após o outro atribuindo o que é captado
.then(repos=>console.log(repos))
.catch(error=>console.log(error)) 
