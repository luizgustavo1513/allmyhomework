const axios = require('axios')

async function start(){

    try {
        const url = 'https://api.github.com/users/maykbrito'
        const res = await axios.get(url)
        const repos = await axios.get(res.data.repos_url)
        console.log(repos.data)
    }

    catch(e){
        console.log(e)
    }
}

start()

axios.get("https://api.github.com/users/maykbrito")
.then(res => axios.get(res.data.repos_url)) //mais organizado, executa um then após o outro atribuindo o que é captado
.then(repos=>console.log(repos.data))
.catch(error=>console.log(error)) 