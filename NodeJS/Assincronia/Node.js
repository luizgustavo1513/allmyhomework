const https = require('https')
const API = 'https://jsonplaceholder.typicode.com/users?_limit=2'

https.get(API, res =>{
    console.log(res.statusCode)
}) //função callback demora um pouco a mais em relação ao resto do código

console.log('Conectando API')