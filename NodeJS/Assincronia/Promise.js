let aceitar = true

console.log('Pedir Uber')

const fingerPromise = new Promise((resolve, reject)=>{
    
    if(aceitar==true){
        return resolve('O carro chegou')
    }
    else if(aceitar==false){
        return reject('O pedido foi negado')
    }

})

console.log('aguardando')

fingerPromise
    .then(result=>console.log(result)) //acontece se há retorno no primeiro parametro ou Acerto
    .catch(erro=>console.log(erro)) //acontece se há retorno no segundo parametro ou Erro
    .finally(()=>console.log('Requisição finalizada')) //acontece independente do retorno do primeiro ou segundo parametro
