const express = require("express")
const { v4:uuidv4 } = require("uuid")

const app = express();

app.use(express.json()) //midleware para definir tipos de dados aceitos

const customers = [];

//MIDDLEWARE => Função que se repetiria em varias rotas que é inserida externamente. ex: 

function verifyAccount(req, res, next){
    
    const { cpf } = req.headers;

    const customer = customers.find(customer => customer.cpf === cpf) //mesma coisa do some mas ao invés de boolean ele retorna o objeto todo

    req.customer = customer //isso aqui faz com que a variável dentro do middleware possa ser usada na rota

    if (!customer){
        return res.status(400).send({error: "Usuário não cadastrado"})
    }

    return next()
}

function getBalance(statement){
    const balance = statement.reduce((acc, operation)=>{
        if(operation.type==="credit"){
            return acc+operation.deposit
        }
        else{
            return acc-operation.withdraw
        }
    }, 0)

    return balance
}
//Middlewares podem ser inseridos em funções específicas entre a rota e os parametros req e res ou atribuidos a todas as rotas por meio do app.use(MIDDLEWARE)

/**
 * cpf - string
 * name - string
 * id - uuid
 * statement - [] (lançamentos de débitos e créditos)
 */
app.post("/signin", (request, response) => { //Deve ser possível criar uma conta
    const {cpf, name} = request.body;

    const customerAlreadyExists = customers.some((customers => customers.cpf === cpf))
    // variável que vai carregar o resultado da função some, que é um boolean(true or false) que percorre o vetor atrás do parametro passado

    if (customerAlreadyExists) {
        return response.status(400).send({error: "CPF já cadastrado"})
    }

    customers.push({
        name,
        cpf,
        id: uuidv4(),
        statement: []
    });
    return response.status(201).send() //status 201 = cadastro registrado 
})

app.get("/statement", verifyAccount, (req, res) => {
    const {customer} = req //isso aqui faz com que a variável dentro do middleware possa ser usada na rota
    return  res.json(customer.statement)
})

app.post("/deposit", verifyAccount, (req,res)=> {
    const {deposit, description} = req.body;
    const {customer} = req;

    const statementOp = {
        description,
        deposit,
        created_at: new Date(),
        type: "credit"
    }

    customer.statement.push(statementOp)
    return res.status(201).send({result: "Depósito realizado"})
})

app.post("/withdraw", verifyAccount, (req,res) => {
    const {withdraw, description} = req.body;
    const {customer} = req;
    
    const balance = getBalance(customer.statement)
    
    if(balance<withdraw){
        return res.status(400).send({Error:"Fundos insuficientes"})
    }

    const statementOp = {
        description,
        withdraw,
        created_at: new Date(),
        type: "debit"
    }

    customer.statement.push(statementOp)

    return res.status(201).send({Result: "Saque realizado"})
})

app.get("/statement/date", verifyAccount, (req,res) => {
    const {customer} = req;
    const {date} = req.query;

    const dateFormat = new Date(date + " 00:00") //faz com que a hora seja 0 para toda data, assim qualquer pesquisa por data não terá problema com horas e minutos

    const statement = customer.statement.filter(
        (statement) => 
            statement.created_at.toDateString() === 
            new Date(dateFormat).toDateString())

    return res.json(statement)
})

app.put("/update", verifyAccount, (req,res) => {
    const { name } = req.body;
    const { customer } = req;

    customer.name = name

    return res.status(202).send({Result: "Update succesfull"})
})

app.get("/users", (req,res) => {
    return res.status(200).send(customers)
})

app.get("/user", verifyAccount, (req,res) => {
    const { customer } = req
    return res.status(200).send(customer)
})

app.delete("/delete", verifyAccount, (req,res) => {
    const { customer } = req

    customers.splice(customer, 1) //splice é uma função que remove itens de um array a partir do primeiro parametro e remove o total passado pelo segundo parametro
    return res.status(200).json(customers)
})

app.get("/balance", verifyAccount, (req,res) => {
    const {customer} = req
    const balance = getBalance(customer.statement)
    return res.status(200).send({Result: `O balanço de ${customer.name} é ${balance}`})
})

app.listen(3333)