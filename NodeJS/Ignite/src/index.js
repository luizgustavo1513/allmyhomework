const express = require("express")
const app = express()
app.use(express.json()) //serve ṕra especificar que eu recebo json na minha aplicação

// Tipos de Parametros: Route, Query, body

app.get("/courses", (req,res) => {
    return res.json([
        "Curso 1",
        "Curso 2",
        "Curso 3"
    ])
})

app.post('/courses', (req,res) => {
    return res.json(['Curso 1', "Curso 2", "Curso 3", "Curso 4"])
})

app.put("/courses/:id", (req,res) => {
    return res.json(['Curso 6', 'Curso 5', 'Curso 4', "Curso 3"])
})

app.patch('/courses/:id', (req,res) => {
    return res.json(['Curso 6', 'Curso 5', 'Curso 7', "Curso 3"])
})

app.delete('/courses/:id', (req,res) => {
    return res.json(['Curso 6', 'Curso 5', 'Curso 4'])
})

app.listen(3333)