const express = require("express")
const app = express()
const axios = require('axios')

app.listen('3000')

app.use(express.json())

app.route('/').get((req,res)=>{
    const url  = 'https://api.github.com/users/Jakeliny'
    axios.get(url).then(result => res.send(`<img src="${result.data.avatar_url}"/>`)).catch(e=>console.log(e)) //renderiza a imagem do perfil como resposta a requisição, em html
})