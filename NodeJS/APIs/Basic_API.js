const express = require("express")

const app = express()

app.listen('3000')

let author = "Luiz Gustavo Peruchi"

authorList = [author]

// --> app.route('/').get((req,res)=>{ res.send('Hello') }) //coloca a resposta da rota get na arrow function
// --> app.route('/sobre').get((req,res)=>{ res.send('Hello sobre') }) //a rota depois do / é específica e tem uma resposta

//GET requisita informações 

//middleware = necessário sempre que estivermos fazendo requisição, como pelo body

app.use(express.json()) //transforma tudo em json

//Post escreve e altera informações

app.route('/').post((req,res)=> {console.log(req.body), res.send(req.body.name), authorList.push(req.body.name)}) 
//navegador não consegue fazer post / print feito pelo post do Insomnia / retorna o valor do req.body

//Put altera informações

app.route("/").put( (req,res) => {
    authorList[0] = req.body.name
    res.send(authorList)
})

app.route("/").get((req,res) => res.send(authorList))

app.route('/:identificador').delete( (req,res) => {
    res.send(req.params.identificador)
    authorList.pop()})