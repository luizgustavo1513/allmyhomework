const express = require("express")

const app = express()

app.listen('3000')

app.use(express.json())

//Body Params

app.route("/").post((req,res)=>{
    const {name, VN, Rating} = req.body //pede para que sejam passados parametros no corpo da requisição para serem comitados no backend
    res.send(`A personagem ${name} de ${VN} é nota ${Rating}`)
})



//Route Params

// app.route("/").get((req,res)=>{
//     res.send( "Olá" )
// })

app.route("/:nome").get((req,res)=>{
    res.send( req.params.nome )
})

app.route("/:nome/:auth").get((req,res)=>{
    res.send( req.params.auth )
})

//Query params

app.route("/").get((req,res)=>{
    res.send(req.query.Idade) //variáveis colocadas na url pelo símbolo de ? ex: ...?name=Luiz. e podem ser colocados multiplas variaveis com & ex: ...?name=Luiz&Idade=21
})