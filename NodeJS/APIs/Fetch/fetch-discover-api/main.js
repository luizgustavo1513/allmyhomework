const url ='http://localhost:5500/api'

//fetch não só barra na url e pede conteúdo como também manda para lá

function getUsers() {
    fetch(url) //busca conteúdo da url especificada
        .then(reponse => reponse.json())
        .then(data => renderApiResult.textContent = JSON.stringify(data)) //se a arrow function retornasse console.log(response.json()) esse then receberia isso também / precisa de stringfy porque a API retorna um json
        .catch(error => console.log(error))
}

function getUser(id){
    fetch(`${url}/${id}`) //busca informações da URL em seu primeiro objeto
        .then(response => response.json())
        .then(data => {
            userName.textContent = data.name
            userCity.textContent = data.city //não precisa de stringfy pois está pegando informações específicas da classe 1 especificada no url
            userAvatar.src = data.avatar
        })
        .catch(error => console.log(error))
}

function addUser(newUser){
    fetch(url, {
        method: "POST", //se não específicado é get por padrão
        body: JSON.stringify(newUser),
        headers: {
            'Content-type': 'application/json; charset=UTF-8'
        }
    })
        .then(response => response.json())
        .then(data => alert.textContent = data) //não precisa de stringfy porque na API ela retorna texto simples
        .catch(e => console.error(e))
}

function updateUser(updatedUser, id){
    fetch(`${url}/${id}`, { //a maneira de se passar o id pela rota é específico dessa API e varia de acordo com a programação e documentação de cada API
        method: "PUT",
        body: JSON.stringify(updatedUser), //transforma o objeto numa string sem perder os elementos de objeto
        headers: {
            'Content-type': 'application/json; charset=UTF-8'
        } //header precisam ser especificados exceto no get que já vem padronizado
    })
        .then(response => response.json())
        .then(data => alert.textContent = data) //não precisa de stringfy porque na API ela retorna texto simples
        .catch(error => console.log(error))
}

function deleteUser(id){
    fetch(`${url}/${id}`,{
        method: "DELETE",
        headers: {'Content-type': 'application/json; charset=UTF-8'}
    })
        .then(reponse => reponse.json())
        .then( data => alert.textContent = data)
        .catch(error => console.error(error))
}

const newUser = {
    name: "Luiz Gustavo",
    avatar: "https://picsum.photos/200/300",
    city: "Turvo"
}

const updatedUser = {
    name: "Marcelo Bez Batti",
    avatar: "https://picsum.photos/200/300",
    city: 'Criciúma'
 }

const id = 5

addUser(newUser)
updateUser(updatedUser, id)

getUsers()
getUser(id)
deleteUser(5)