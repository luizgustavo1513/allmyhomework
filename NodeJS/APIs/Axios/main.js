const url = "http://localhost:5500/api"

function getUsers(){
    axios.get(url)
        .then(response => apiResult.textContent = JSON.stringify(response.data))
        .catch(error => console.error(error))
}

getUsers()

//---------------------------------------------------//

const newUser = {
    name: "Luiz Gustavo",
    avatar: "https://picsum.photos/200/300",
    city: "Criciúma"
}

function postUser(newUser){
    axios.post(url, newUser)
        .then(response => console.log(response))
        .catch(error => console.error(error))
}

// postUser(newUser)

//---------------------------------------------------//

const modifiedUser = {
    name: "Inácio da Silva",
    avatar: "https://picsum.photos/200/300",
    city: "Timbé do Sul"
}

function putUser(id, modifiedUser){
    axios.put(`${url}/${id}`, modifiedUser)
        .then(reponse => console.log(reponse))
        .catch(error => console.error(error))
}

putUser(2, modifiedUser)

//---------------------------------------------------//

function getUserInfo(Id){
    axios.get(`${url}/${Id}`)
        .then(response => response.data)
        .then(data => {
            userName.textContent = data.name
            userCity.textContent = data.city
            userId.textContent = data.id
            userAvatar.src = data.avatar
        })
        .catch(error => console.error(error))
}

getUserInfo(3)

//---------------------------------------------------//

function deleteUser(id){
    axios.delete(`${url}/${id}`)
        .then(response => console.log(response))
        .catch(error => console.error(error))
}

deleteUser(9)

//---------------------------------------------------//


