//var é global, let é local

console.log(x) //hoisting, ele define x mesmo antes de eu declarar ele por var ser global. Ele joga pra cima antes do código apenas para declara existência e depois ele declara o valor 

let y = 3

// {} = bloco que declara um novo scopo

{
    var x = 0
    y = 9 // nesse caso ele atualiza o valor de y mesmo fora do scopo onde foi declarada a mudança
    {
        console.log(y) // Let não funciona pra scopos externos mas internos sim
    }
    console.log(y) //ele imprime aqui por estar no scopo
}

console.log(x)
console.log(y) //O y não existe nesse scopo