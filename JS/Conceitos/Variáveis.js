var clima = "Quente" //Atribui o valor 

console.log(clima) //Imprime

clima = "Frio" //Muda o valor

console.log(clima) //Imprime

let roupa = "Grande"

console.log(roupa)

roupa = "Pequena"

console.log(roupa)

const trem = "Aço"

console.log(trem)

trem = "Ferro" //Const é a única que não pode mudar e dá erro

console.log(trem) //Não imprime por causa do erro

let calo

console.log(calo) //undefined

let tot =''

console.log(typeof tot) //string