import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from 'typeorm';

export class CreateCarsSpecifications1653485512921
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'cars_specifications',
        columns: [
          {
            name: 'car_id',
            type: 'uuid',
            isNullable: true,
          },
          {
            name: 'specification_id',
            type: 'uuid',
            isNullable: true,
          },
          {
            name: 'created_at',
            type: 'timestamp',
            default: 'now()',
          },
        ],
      }),
    );
    await queryRunner.createForeignKey(
      'cars_specifications',
      new TableForeignKey({
        name: 'FKSpecificationId',
        referencedTableName: 'specifications',
        referencedColumnNames: ['id'],
        columnNames: ['specification_id'],
        onDelete: 'SET NULL',
        onUpdate: 'SET NULL',
      }),
    );
    await queryRunner.createForeignKey(
      'cars_specifications',
      new TableForeignKey({
        name: 'FKCarId',
        referencedTableName: 'Cars',
        referencedColumnNames: ['id'],
        columnNames: ['car_id'],
        onDelete: 'SET NULL',
        onUpdate: 'SET NULL',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey('cars_specifications', 'FKCarId');
    await queryRunner.dropForeignKey(
      'cars_specifications',
      'FKSpecificationId',
    );
    await queryRunner.dropTable('cars_specifications');
  }
}
