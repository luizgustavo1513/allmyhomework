import { hash } from 'bcrypt';
import { v4 as uuidv4 } from 'uuid';

import createConnection from '../index';

async function create() {
  const connection = await createConnection('localhost');

  const id = uuidv4();
  const password = await hash('admin', 8);
  await connection.query(
    `INSERT INTO USERS(id,name,username,password, admin, created_at, driver_license ) 
    values('${id}', 'admin', 'adminuser', '${password}', true, 'now()', 'xxxxxxx')
    `,
  );
  await connection.close;
}
create()
  .then(() => console.log('User Admin created!'))
  .catch(e => console.log(`não sei o que aconteceu - ${e}`));
