import express, { NextFunction, Request, Response } from 'express';
import 'express-async-errors';
import swaggerUi from 'swagger-ui-express';

import 'reflect-metadata';
import swaggerFile from '../../../swagger.json';
import { AppError } from '../../errors/AppErros';
import '../../container';
import createConnection from '../typeorm';
import { router } from './routes';

const app = express();
// dessa maneira todas as rotas dentro do categories usam como url base o localhost:3333/categories

createConnection();

app.use(express.json());

app.use('/api/docs', swaggerUi.serve, swaggerUi.setup(swaggerFile));

app.use(router);

app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
  if (err instanceof AppError) {
    return res.status(err.statusCode).json({ message: err.message });
  }
  return res.status(500).json({
    status: 'error',
    message: `Internal server Error - ${err.message} `,
  });
});

export { app };
