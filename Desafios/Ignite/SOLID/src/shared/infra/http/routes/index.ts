import { Router } from 'express';

import { authRoutes } from './authenticate.routes';
import { carRoutes } from './car.routes';
import { categoriesRoutes } from './categories.routes';
import { rentalRoutes } from './rental.routes';
import { specsRoutes } from './specs.routes';
import { userRoutes } from './user.routes';

const router = Router();

router.use(authRoutes);
router.use('/users', userRoutes);
router.use('/specs', specsRoutes);
router.use('/categories', categoriesRoutes);
router.use('/cars', carRoutes);
router.use('/rentals', rentalRoutes);

export { router };
