import { Router } from 'express';
import multer from 'multer';

import { CreateCategoryController } from '../../../../modules/cars/useCases/createCategory/createCategoryController';
import { ImportCategoryController } from '../../../../modules/cars/useCases/importCategory/importCategoryController';
import { ListCategoryController } from '../../../../modules/cars/useCases/listCategories/listCategoryContoller';
import { ensureAdmin } from '../middlewares/ensureAdmin';
import { ensureAuthentication } from '../middlewares/ensureAuthenticate';

const categoriesRoutes = Router();
const upload = multer({
  // biblioteca responsável por pegar os arquivos e fazer o upload por meio de uma função middleware na aplicação
  dest: './tmp',
});

const createCategoryController = new CreateCategoryController();
const importCategoryController = new ImportCategoryController();
const listCategoryContoller = new ListCategoryController();

categoriesRoutes.post(
  '/',
  ensureAuthentication,
  ensureAdmin,
  createCategoryController.handle,
);

categoriesRoutes.get('/', listCategoryContoller.handle);

categoriesRoutes.post(
  '/import',
  ensureAuthentication,
  ensureAdmin,
  upload.single('file'),
  importCategoryController.handle,
);

export { categoriesRoutes };
