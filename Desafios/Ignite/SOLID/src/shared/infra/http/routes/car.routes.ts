import { Router } from 'express';
import multer from 'multer';

import upload from '../../../../config/upload';
import { CreateCarController } from '../../../../modules/cars/useCases/createCar/createCarController';
import { CreateCarSpecificationController } from '../../../../modules/cars/useCases/createCarSpecification/createCarSpecificationController';
import { ListCarsControllers } from '../../../../modules/cars/useCases/listAvailableCars/listCarsController';
import { UploadCarImageController } from '../../../../modules/cars/useCases/uploadCarImage/UploadCarImageController';
import { ensureAdmin } from '../middlewares/ensureAdmin';
import { ensureAuthentication } from '../middlewares/ensureAuthenticate';

const carRoutes = Router();

const carContoller = new CreateCarController();
const listAvailableCars = new ListCarsControllers();
const createCarSpecificationController = new CreateCarSpecificationController();
const carImagesController = new UploadCarImageController();

const uploadImages = multer(upload.upload('./tmp/cars'));

carRoutes.post('/', ensureAuthentication, ensureAdmin, carContoller.handle);

carRoutes.get('/available', listAvailableCars.handle);

carRoutes.post(
  '/specifications/:id',
  ensureAuthentication,
  ensureAdmin,
  createCarSpecificationController.handle,
);

carRoutes.post(
  '/images/:id',
  ensureAuthentication,
  ensureAdmin,
  uploadImages.array('images'),
  carImagesController.handle,
);
export { carRoutes };
