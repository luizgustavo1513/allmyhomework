import { Router } from 'express';
import multer from 'multer';

import uploadConfig from '../../../../config/upload';
import { CreateUserController } from '../../../../modules/accounts/useCases/createUser/createUserController';
import { ListUserController } from '../../../../modules/accounts/useCases/listUser/listUserController';
import { UpdateUserAvatarController } from '../../../../modules/accounts/useCases/updateUserAvatar/updateUserAvatarController';
import { ensureAuthentication } from '../middlewares/ensureAuthenticate';

const userRoutes = Router();
const uploadAvatar = multer(uploadConfig.upload('./tmp/avatar'));

const createUser = new CreateUserController();
const listUser = new ListUserController();
const updateUserAvatar = new UpdateUserAvatarController();

// userRoutes.use(ensureAuthentication);

userRoutes.post('/', createUser.handle);

userRoutes.get('/', listUser.handle);

userRoutes.patch(
  '/avatar',
  ensureAuthentication,
  uploadAvatar.single('avatar'),
  updateUserAvatar.handle,
);

export { userRoutes };
