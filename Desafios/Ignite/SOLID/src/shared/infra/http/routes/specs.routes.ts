import { Router } from 'express';

import { CreateSpecController } from '../../../../modules/cars/useCases/createSpec/createSpecController';
import { ListSpecController } from '../../../../modules/cars/useCases/listSpecs/listSpecsController';
import { ensureAuthentication } from '../middlewares/ensureAuthenticate';

const specsRoutes = Router();

const createSpecController = new CreateSpecController();
const listSpecController = new ListSpecController();

specsRoutes.use(ensureAuthentication);
specsRoutes.post('/', createSpecController.handle);

specsRoutes.get('/', listSpecController.handle);

export { specsRoutes };
