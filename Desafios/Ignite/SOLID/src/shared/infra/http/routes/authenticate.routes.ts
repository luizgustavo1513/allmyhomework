import { Router } from 'express';

import { AuthenticateUserController } from '../../../../modules/accounts/useCases/authenticateUser/authenticateUserController';

const authRoutes = Router();

const AuthUserController = new AuthenticateUserController();

authRoutes.post('/sessions', AuthUserController.handle);

export { authRoutes };
