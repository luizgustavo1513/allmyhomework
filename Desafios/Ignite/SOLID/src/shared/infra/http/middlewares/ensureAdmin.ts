import { NextFunction, Request, Response } from 'express';

import { PostgresUserRepositories } from '../../../../modules/accounts/infra/typeorm/repositories/PostgresUserRepository';
import { AppError } from '../../../errors/AppErros';

export async function ensureAdmin(
  req: Request,
  res: Response,
  next: NextFunction,
) {
  const { id } = req.user;
  const userRepository = new PostgresUserRepositories();

  const user = await userRepository.findById(id);

  if (user === undefined || !user.admin) {
    throw new AppError("User isn't admin");
  }
  return next();
}
