import { NextFunction, Request, Response } from 'express';
import { verify } from 'jsonwebtoken';

import { PostgresUserRepositories } from '../../../../modules/accounts/infra/typeorm/repositories/PostgresUserRepository';
import { AppError } from '../../../errors/AppErros';

interface IPayload {
  sub: string;
}

export async function ensureAuthentication(
  req: Request,
  res: Response,
  next: NextFunction,
) {
  // try {
  const authHeader = req.headers.authorization;

  if (!authHeader) {
    throw new AppError('Token missing', 401);
  }

  const [, token] = authHeader.split(' ');

  try {
    const { sub: user_id } = verify(
      token,
      'b6398490cb096add228abc6afb306c2e',
    ) as IPayload;
    const userRepository = new PostgresUserRepositories();
    const user = await userRepository.findById(user_id);

    if (!user) {
      throw new AppError('User does not exist!', 401);
    }

    // tipo reescrito na pasta @types pois normalmente o tipo request não tem propriedade user
    req.user = {
      id: user_id,
    };

    return next();
  } catch (e) {
    return res.status(400).json({ message: (e as Error).message });
  }
}
