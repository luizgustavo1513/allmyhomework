import { container } from 'tsyringe';

import { PostgresUserRepositories } from '../../modules/accounts/infra/typeorm/repositories/PostgresUserRepository';
import { IUsersRepository } from '../../modules/accounts/repositories/IUsersRepository';
import { PostgresCarsImagesRepository } from '../../modules/cars/infra/typeorm/repositories/PostgresCarsImagesRepository';
import { PostgresCarsRepositories } from '../../modules/cars/infra/typeorm/repositories/PostgresCarsRepositories';
import { PostgressCategoriesRepositories } from '../../modules/cars/infra/typeorm/repositories/PostgresCategoriesRepositories';
import { PostgresSpecsRepositories } from '../../modules/cars/infra/typeorm/repositories/PostgresSpecsRepositories';
import { ICarsImagesRepositories } from '../../modules/cars/repositories/ICarsImagesRepositories';
import { ICarsRepositories } from '../../modules/cars/repositories/ICarsRepositories';
import { ICategoriesRepositories } from '../../modules/cars/repositories/ICategoriesRepositories';
import { ISpecRepositories } from '../../modules/cars/repositories/ISpecRepositories';
import { PostgresRentalRepository } from '../../modules/rentals/infra/typeorm/repositories/PostgresRentalsRepository';
import { IRentalRepositories } from '../../modules/rentals/repositories/IRentalRepositories';
import { IDateProvider } from './providers/DateProvider/IDateProvider';
import { DayJS } from './providers/DateProvider/implementations/DayJS';

// ICategoriesRepository
container.registerSingleton<ICategoriesRepositories>( // classe a ser atribuida
  'PostgressCategoriesRepositories', // nome do container a ser chamado pelo inject
  PostgressCategoriesRepositories, // classe a receber atribuições
);

container.registerSingleton<ISpecRepositories>(
  'SpecsRepository',
  PostgresSpecsRepositories,
);

container.registerSingleton<IUsersRepository>(
  'PostgresUserRepositories',
  PostgresUserRepositories,
);

container.registerSingleton<ICarsRepositories>(
  'PostGresCarsRepo',
  PostgresCarsRepositories,
);

container.registerSingleton<ICarsImagesRepositories>(
  'CarsImagesRepository',
  PostgresCarsImagesRepository,
);

container.registerSingleton<IRentalRepositories>(
  'PostgresRentalRepo',
  PostgresRentalRepository,
);

container.registerSingleton<IDateProvider>('DateProvider', DayJS);
