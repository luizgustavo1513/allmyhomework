import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';

import { IDateProvider } from '../IDateProvider';

dayjs.extend(utc);

class DayJS implements IDateProvider {
  async convertToUTC(date: Date): Promise<string> {
    const dayUTC = dayjs(date).utc().local().format();
    return dayUTC;
  }
  async compareInHours(start_date: Date, end_date: Date): Promise<number> {
    const end_date_format = await this.convertToUTC(end_date);
    const start_date_format = await this.convertToUTC(start_date);
    return dayjs(end_date_format).diff(start_date_format, 'hours');
  }
  dateNow(): Date {
    return dayjs().toDate();
  }
  async compareInDays(start_date: Date, end_date: Date): Promise<number> {
    const end_date_format = await this.convertToUTC(end_date);
    const start_date_format = await this.convertToUTC(start_date);
    return dayjs(end_date_format).diff(start_date_format, 'days');
  }
}

export { DayJS };
