interface IDateProvider {
  compareInHours(start_date: Date, end_date: Date): Promise<number>;
  convertToUTC(date: Date): Promise<string>;
  dateNow(): Date;
  compareInDays(start_date: Date, end_date: Date): Promise<number>;
}

export { IDateProvider };
