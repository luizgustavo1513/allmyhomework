import { getRepository, Repository } from 'typeorm';

import { ICreateRentalDTO } from '../../../DTO/RentalDTO';
import { IRentalRepositories } from '../../../repositories/IRentalRepositories';
import { Rental } from '../entities/rental';

class PostgresRentalRepository implements IRentalRepositories {
  private repository: Repository<Rental>;

  constructor() {
    this.repository = getRepository(Rental);
  }

  async create(data: ICreateRentalDTO): Promise<Rental> {
    const rent = this.repository.create({
      car_id: data.car_id,
      expected_return_date: data.expected_return_date,
      user_id: data.user_id,
    });

    await this.repository.save(rent);

    return rent;
  }
  async findByCar(car_id: string): Promise<Rental | undefined> {
    const rent = await this.repository.findOne({ car_id });
    return rent;
  }
  async findByUser(user_id: string): Promise<Rental | undefined> {
    const rent = await this.repository.findOne({ user_id });
    return rent;
  }
  async findById(id: string): Promise<Rental | undefined> {
    const rent = await this.repository.findOne(id);
    return rent;
  }
}

export { PostgresRentalRepository };
