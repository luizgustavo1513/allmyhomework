import { ICreateRentalDTO } from '../DTO/RentalDTO';
import { Rental } from '../infra/typeorm/entities/rental';

interface IRentalRepositories {
  create(data: ICreateRentalDTO): Promise<Rental>;
  findByCar(car_id: string): Promise<Rental | undefined>;
  findByUser(user_id: string): Promise<Rental | undefined>;
  findById(rental_id: string): Promise<Rental | undefined>;
}

export { IRentalRepositories };
