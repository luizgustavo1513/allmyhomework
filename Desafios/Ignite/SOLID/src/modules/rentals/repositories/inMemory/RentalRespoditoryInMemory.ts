import { ICreateRentalDTO } from '../../DTO/RentalDTO';
import { Rental } from '../../infra/typeorm/entities/rental';
import { IRentalRepositories } from '../IRentalRepositories';

class RentalRepositoryInMemory implements IRentalRepositories {
  Rentals: Rental[] = [];

  async create({
    user_id,
    car_id,
    expected_return_date,
  }: ICreateRentalDTO): Promise<Rental> {
    const rent = new Rental();

    Object.assign(rent, {
      user_id,
      car_id,
      expected_return_date,
      start_date: new Date(),
    });

    this.Rentals.push(rent);

    return rent;
  }
  async findByCar(car_id: string): Promise<Rental | undefined> {
    const rent = await this.Rentals.find(
      car => car.car_id === car_id && !car.end_date,
    );
    return rent;
  }
  async findByUser(user_id: string): Promise<Rental | undefined> {
    const rent = await this.Rentals.find(
      rent => rent.user_id === user_id && !rent.end_date,
    );
    return rent;
  }
}

export { RentalRepositoryInMemory };
