import { inject } from 'tsyringe';
import { IDateProvider } from '../../../../shared/container/providers/DateProvider/IDateProvider';
import { AppError } from '../../../../shared/errors/AppErros';
import { ICarsRepositories } from '../../../cars/repositories/ICarsRepositories';
import { IRentalRepositories } from '../../repositories/IRentalRepositories';

class DevolutionRentalUseCase {
  constructor(
    @inject('PostgresRentalRepo')
    private rentalsRepository: IRentalRepositories,
    @inject('PostGresCarsRepo')
    private carsRepository: ICarsRepositories,
    @inject('DateProvider')
    private DateProvider: IDateProvider,
  ) {}
  async execute(rental_id: string, car_id: string) {
    const rental = await this.rentalsRepository.findById(rental_id);
    const car = await this.carsRepository.findById(car_id);
    const minimum_daily = 1;

    if (!car) {
      throw new AppError('Car does not exist!');
    }

    if (!rental) {
      throw new AppError('Rental does not exist!');
    }

    const dateNow = this.DateProvider.dateNow();

    let daily = await this.DateProvider.compareInDays(
      rental.start_date,
      this.DateProvider.dateNow(),
    );

    if (daily <= 0) {
      daily = minimum_daily;
    }

    const delay = await this.DateProvider.compareInDays(
      dateNow,
      rental.expected_return_date,
    );

    if (delay > 0) {
      const calculate_fine = delay * car.fine_amount;
    }
  }
}

export { DevolutionRentalUseCase };
