import dayjs from 'dayjs';

import { DayJS } from '../../../../shared/container/providers/DateProvider/implementations/DayJS';
import { AppError } from '../../../../shared/errors/AppErros';
import { CarsRepoInMemory } from '../../../cars/repositories/in-memory/carsRepositoryInMemory';
import { RentalRepositoryInMemory } from '../../repositories/inMemory/RentalRespoditoryInMemory';
import { CreateRentalUseCase } from './CreateRentalUseCase';

let createRentalUseCase: CreateRentalUseCase;
let RentalRepo: RentalRepositoryInMemory;
let carsRepo: CarsRepoInMemory;
let DateRepo: DayJS;

describe('Create Rental', () => {
  const dayAfter = dayjs().add(1, 'day').toDate();
  beforeEach(() => {
    RentalRepo = new RentalRepositoryInMemory();
    DateRepo = new DayJS();
    carsRepo = new CarsRepoInMemory();
    createRentalUseCase = new CreateRentalUseCase(
      RentalRepo,
      DateRepo,
      carsRepo,
    );
  });

  it('should be able to create a new rental', async () => {
    const rent = await createRentalUseCase.execute({
      car_id: '1234',
      expected_return_date: dayAfter,
      user_id: '34234',
    });

    expect(rent).toHaveProperty('id');
    expect(rent).toHaveProperty('start_date');
  });

  it('Shouldn"t be able to rent if the user is already renting', async () => {
    expect(async () => {
      await createRentalUseCase.execute({
        car_id: '123445',
        expected_return_date: dayAfter,
        user_id: '34234',
      });
      await createRentalUseCase.execute({
        car_id: '1234',
        expected_return_date: dayAfter,
        user_id: '34234',
      });
    }).rejects.toBeInstanceOf(AppError);
  });
  it('Shouldn"t be able to rent if the user is already renting', async () => {
    expect(async () => {
      await createRentalUseCase.execute({
        car_id: '1234',
        expected_return_date: dayAfter,
        user_id: '3423434',
      });
      await createRentalUseCase.execute({
        car_id: '1234',
        expected_return_date: dayAfter,
        user_id: '34234',
      });
    }).rejects.toBeInstanceOf(AppError);
  });
  it('Shouldnt be able to create a rental with less than 24 hours', async () => {
    expect(async () => {
      await createRentalUseCase.execute({
        car_id: '12343',
        user_id: '12412',
        expected_return_date: dayjs().toDate(),
      });
    }).rejects.toBeInstanceOf(AppError);
  });
});
