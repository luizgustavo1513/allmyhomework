import { Request, Response } from 'express';
import { container } from 'tsyringe';
import { CreateRentalUseCase } from './CreateRentalUseCase';

class CreateRentalController {
  async handle(req: Request, res: Response): Promise<Response> {
    const { expected_return_date, car_id } = req.body;
    const { id } = req.user;
    const rentalUseCase = container.resolve(CreateRentalUseCase);

    const rental = await rentalUseCase.execute({
      car_id,
      user_id: id,
      expected_return_date,
    });

    return res.status(200).json(rental);
  }
}

export { CreateRentalController };
