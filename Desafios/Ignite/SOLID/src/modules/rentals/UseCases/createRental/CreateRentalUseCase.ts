import { inject, injectable } from 'tsyringe';
import { IDateProvider } from '../../../../shared/container/providers/DateProvider/IDateProvider';
import { AppError } from '../../../../shared/errors/AppErros';
import { ICarsRepositories } from '../../../cars/repositories/ICarsRepositories';
import { ICreateRentalDTO } from '../../DTO/RentalDTO';
import { Rental } from '../../infra/typeorm/entities/rental';
import { IRentalRepositories } from '../../repositories/IRentalRepositories';

@injectable()
class CreateRentalUseCase {
  constructor(
    @inject('PostgresRentalRepo')
    private rentalsRepository: IRentalRepositories,
    @inject('DateProvider')
    private DateProvider: IDateProvider,
    @inject('PostGresCarsRepo')
    private carsRepository: ICarsRepositories,
  ) {}
  async execute({
    user_id,
    car_id,
    expected_return_date,
  }: ICreateRentalDTO): Promise<Rental> {
    const MinimunHour = 24;
    const carUnavailability = await this.rentalsRepository.findByCar(car_id);

    if (carUnavailability) {
      throw new AppError('Car is unavailable!');
    }
    const userUnavailability = await this.rentalsRepository.findByUser(user_id);

    if (userUnavailability) {
      throw new AppError('User is already renting!');
    }

    const dateNow = this.DateProvider.dateNow();

    const date_total = await this.DateProvider.compareInHours(
      dateNow,
      expected_return_date,
    );

    if (date_total < MinimunHour) {
      throw new AppError('time to rent is less than 24 hours ');
    }

    const rent = await this.rentalsRepository.create({
      user_id,
      car_id,
      expected_return_date,
    });

    await this.carsRepository.updateAvailable(car_id, false);

    return rent;
  }
}
export { CreateRentalUseCase };
