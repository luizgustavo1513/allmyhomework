import { inject, injectable } from 'tsyringe';

import { AppError } from '../../../../shared/errors/AppErros';
import { deleteFile } from '../../../../utils/file';
import { IUsersRepository } from '../../repositories/IUsersRepository';

interface IRequest {
  user_id: string;
  avatarFile?: string;
}

@injectable()
class UpdateUserAvatarUseCase {
  constructor(
    @inject('PostgresUserRepositories')
    private usersRepository: IUsersRepository,
  ) {}
  async execute({ user_id, avatarFile }: IRequest): Promise<void> {
    const user = await this.usersRepository.findById(user_id);

    if (user === undefined) {
      throw new AppError("User doesn't exists");
    }

    await deleteFile(`./tmp/avatar/${user.avatar}`);
    user.avatar = avatarFile;

    await this.usersRepository.create(user);
  }
}

export { UpdateUserAvatarUseCase };
