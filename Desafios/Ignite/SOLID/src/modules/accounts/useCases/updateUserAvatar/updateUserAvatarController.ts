import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { UpdateUserAvatarUseCase } from './updateUserAvatarUseCase';

class UpdateUserAvatarController {
  async handle(req: Request, res: Response): Promise<Response> {
    const { id } = req.user;
    // const { avatarFile } = req;

    const avatarFile = req.file?.filename;

    const updateUserAvatarUseCase = container.resolve(UpdateUserAvatarUseCase);

    const Avatar = await updateUserAvatarUseCase.execute({
      user_id: id,
      avatarFile,
    });

    return res.status(200).json({ Avatar });
  }
}

export { UpdateUserAvatarController };
