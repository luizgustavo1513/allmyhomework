import { compare } from 'bcrypt';
import { sign } from 'jsonwebtoken';
import { inject, injectable } from 'tsyringe';

import { AppError } from '../../../../shared/errors/AppErros';
import { IUsersRepository } from '../../repositories/IUsersRepository';

interface IRequest {
  username: string;
  password: string;
}

interface IResponse {
  user: {
    name: string;
    username: string;
  };
  token: string;
}

@injectable()
class AuthenticateUserUseCase {
  constructor(
    @inject('PostgresUserRepositories')
    private userRepository: IUsersRepository,
  ) {}
  async execute({ username, password }: IRequest): Promise<IResponse> {
    const user = await this.userRepository.getUserByUserName(username);
    if (!user) {
      throw new AppError('User or password incorrect');
    }

    const passwordVerify = await compare(password, user.password);

    if (!passwordVerify) {
      throw new AppError('User or password incorrect');
    }

    const token = sign({}, 'b6398490cb096add228abc6afb306c2e', {
      subject: user.id,
      expiresIn: '1d',
    });

    const tokenReturn: IResponse = {
      token,
      user: {
        name: user.name,
        username: user.username,
      },
    };

    return tokenReturn;
  }
}

export { AuthenticateUserUseCase };
