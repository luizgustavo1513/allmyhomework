import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { AuthenticateUserUseCase } from './authenticateUserUseCase';

class AuthenticateUserController {
  async handle(req: Request, res: Response): Promise<Response> {
    const { username, password } = req.body;

    const authUseCase = container.resolve(AuthenticateUserUseCase);

    // try {
    const token = await authUseCase.execute({ password, username });

    return res.json(token);
    // } catch (e) {
    //  return res.status(400).json({ message: (e as Error).message });
    // }
  }
}

export { AuthenticateUserController };
