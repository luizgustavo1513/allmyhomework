import { AppError } from '../../../../shared/errors/AppErros';
import { ICreateUserDTO } from '../../dtos/ICreateUserDTO';
import { UsersRepositoryInMemory } from '../../repositories/In-memory/UsersRepositoryInMemory';
import { CreateUserUserCase } from '../createUser/createUserUseCase';
import { AuthenticateUserUseCase } from './authenticateUserUseCase';

let usersRepository: UsersRepositoryInMemory;
let createUserUseCase: CreateUserUserCase;
let authenticateUserUseCase: AuthenticateUserUseCase;

describe('Authenticate User', () => {
  beforeEach(() => {
    usersRepository = new UsersRepositoryInMemory();

    authenticateUserUseCase = new AuthenticateUserUseCase(usersRepository);

    createUserUseCase = new CreateUserUserCase(usersRepository);
  });

  it('Should be able to authenticate user', async () => {
    const user: ICreateUserDTO = {
      name: 'asda',
      username: 'aaaa',
      password: '1234',
      driver_license: 'sdasdad',
    };

    await createUserUseCase.execute(user);

    const result = await authenticateUserUseCase.execute({
      username: user.username,
      password: user.password,
    });

    expect(result).toHaveProperty('token');
  });

  it('Should not be able to authenticate a nonexistent user', () => {
    expect(async () => {
      await authenticateUserUseCase.execute({
        username: 'alonso',
        password: 'asdada',
      });
    }).rejects.toBeInstanceOf(AppError);
  });

  it('Should not be able to authenticate with incorrect password', async () => {
    const user: ICreateUserDTO = {
      name: 'asdasdsaa',
      username: 'aaavvva',
      password: '1254334',
      driver_license: 'sdasdad',
    };

    await createUserUseCase.execute(user);
    expect(async () => {
      await authenticateUserUseCase.execute({
        username: user.username,
        password: '4321',
      });
    }).rejects.toBeInstanceOf(AppError);
  });

  it('Should not be able to authenticate with incorrect username', async () => {
    const user: ICreateUserDTO = {
      name: 'asdfdsfda',
      username: 'aaaxxxa',
      password: '12312314',
      driver_license: 'sdasdad',
    };

    await createUserUseCase.execute(user);
    expect(async () => {
      await authenticateUserUseCase.execute({
        username: 'bbbb',
        password: user.password,
      });
    }).rejects.toBeInstanceOf(AppError);
  });
});
