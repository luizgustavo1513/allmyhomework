import { hash } from 'bcrypt';
import { inject, injectable } from 'tsyringe';

import { AppError } from '../../../../shared/errors/AppErros';
import { ICreateUserDTO } from '../../dtos/ICreateUserDTO';
import { IUsersRepository } from '../../repositories/IUsersRepository';

@injectable()
class CreateUserUserCase {
  constructor(
    @inject('PostgresUserRepositories')
    private userRepository: IUsersRepository,
  ) {}

  async execute({
    name,
    username,
    password,
    driver_license,
  }: ICreateUserDTO): Promise<void> {
    const user = await this.userRepository.getUserByUserName(username);

    if (user) {
      throw new AppError('Username already registered', 400);
    }

    const passwordHash = await hash(password, 8);

    await this.userRepository.create({
      name,
      username,
      password: passwordHash,
      driver_license,
    });
  }
}

export { CreateUserUserCase };
