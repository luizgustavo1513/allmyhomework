import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { CreateUserUserCase } from './createUserUseCase';

class CreateUserController {
  async handle(req: Request, res: Response): Promise<Response> {
    const { name, username, password, driver_license } = req.body;
    const createUserUseCase = container.resolve(CreateUserUserCase);

    // try {
    await createUserUseCase.execute({
      name,
      username,
      password,
      driver_license,
    });
    // } catch (e) {
    //   return res.status(400).json({ message: (e as Error).message });
    // }
    return res.status(201).send();
  }
}

export { CreateUserController };
