import { inject, injectable } from 'tsyringe';

import { User } from '../../infra/typeorm/entities/user';
import { IUsersRepository } from '../../repositories/IUsersRepository';

@injectable()
class ListUserUseCase {
  constructor(
    @inject('PostgresUserRepositories')
    private UserRepository: IUsersRepository,
  ) {}
  async execute(): Promise<User[]> {
    const list = await this.UserRepository.list();
    return list;
  }
}

export { ListUserUseCase };
