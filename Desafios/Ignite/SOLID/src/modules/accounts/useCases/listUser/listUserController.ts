import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { ListUserUseCase } from './listUserUseCase';

class ListUserController {
  async handle(req: Request, res: Response): Promise<Response> {
    // const { admin } = req.header;

    const listUseCase = container.resolve(ListUserUseCase);

    try {
      const list = await listUseCase.execute();
      return res.status(200).send(list);
    } catch (e) {
      return res.status(400).json({ message: (e as Error).message });
    }
  }
}

export { ListUserController };
