import { ICreateUserDTO } from '../dtos/ICreateUserDTO';
import { User } from '../infra/typeorm/entities/user';

interface IUsersRepository {
  create(data: ICreateUserDTO): Promise<void>;
  getUserByUserName(Username: string): Promise<User | undefined>;
  list(): Promise<User[]>;
  turnAdmin(): void;
  findById(id: string): Promise<User | undefined>;
}

export { IUsersRepository };
