import { ICreateUserDTO } from '../../dtos/ICreateUserDTO';
import { User } from '../../infra/typeorm/entities/user';
import { IUsersRepository } from '../IUsersRepository';

class UsersRepositoryInMemory implements IUsersRepository {
  users: User[] = [];

  async create({
    name,
    username,
    password,
    driver_license,
    avatar,
  }: ICreateUserDTO): Promise<void> {
    const user = new User();

    Object.assign(user, {
      name,
      username,
      password,
      driver_license,
      avatar,
    });

    this.users.push(user);
  }
  async getUserByUserName(username: string): Promise<User | undefined> {
    const user = this.users.find(user => user.username === username);
    return user;
  }
  async list(): Promise<User[]> {
    const all = this.users;
    return all;
  }
  turnAdmin(): void {
    throw new Error('Method not implemented.');
  }
  async findById(id: string): Promise<User | undefined> {
    const user = this.users.find(user => user.id === id);
    return user;
  }
}

export { UsersRepositoryInMemory };
