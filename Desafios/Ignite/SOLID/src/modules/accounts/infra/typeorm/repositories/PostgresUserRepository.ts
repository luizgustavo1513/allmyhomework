import { getRepository, Repository } from 'typeorm';

import { ICreateUserDTO } from '../../../dtos/ICreateUserDTO';
import { IUsersRepository } from '../../../repositories/IUsersRepository';
import { User } from '../entities/user';

class PostgresUserRepositories implements IUsersRepository {
  private repository: Repository<User>;

  constructor() {
    this.repository = getRepository(User);
  }

  async create({
    name,
    username,
    password,
    driver_license,
    avatar,
    id,
  }: ICreateUserDTO): Promise<void> {
    const user = this.repository.create({
      name,
      username,
      password,
      driver_license,
      avatar,
      id,
    });
    await this.repository.save(user);
  }
  async getUserByUserName(username: string): Promise<User | undefined> {
    const user = await this.repository.findOne({ username });
    return user;
  }
  async list(): Promise<User[]> {
    const userlist = await this.repository.find();
    return userlist;
  }
  async turnAdmin(): Promise<void> {}
  async findById(id: string): Promise<User | undefined> {
    const user = await this.repository.findOne(id);
    return user;
  }
}

export { PostgresUserRepositories };
