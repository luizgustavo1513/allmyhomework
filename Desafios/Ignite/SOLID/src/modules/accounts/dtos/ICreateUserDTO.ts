interface ICreateUserDTO {
  name: string;
  username: string;
  password: string;
  driver_license: string;
  avatar?: string;
  id?: string;
}

export { ICreateUserDTO };
