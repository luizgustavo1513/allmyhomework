import { inject, injectable } from 'tsyringe';

import { AppError } from '../../../../shared/errors/AppErros';
import { Car } from '../../infra/typeorm/entities/cars';
import { ICarsRepositories } from '../../repositories/ICarsRepositories';
import { ISpecRepositories } from '../../repositories/ISpecRepositories';

interface IRequest {
  car_id: string | undefined;
  specifications_id: (undefined | string)[];
}

@injectable()
class CreateCarSpecificationUseCase {
  constructor(
    @inject('PostGresCarsRepo')
    private CarsRepo: ICarsRepositories,
    @inject('SpecsRepository')
    private SpecsRepo: ISpecRepositories,
  ) {}
  async execute({ car_id, specifications_id }: IRequest): Promise<Car> {
    const carExists = await this.CarsRepo.findById(car_id);

    if (!carExists) {
      throw new AppError("Car doesn't exists!");
    }
    const specExists = await this.SpecsRepo.findByIds(specifications_id);

    if (!specExists) {
      throw new AppError("Specifications doesn't exists!");
    }

    carExists.specifications = specExists;

    await this.CarsRepo.createCar(carExists);

    return carExists;
  }
}

export { CreateCarSpecificationUseCase };
