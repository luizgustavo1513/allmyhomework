import { AppError } from '../../../../shared/errors/AppErros';
import { CarsRepoInMemory } from '../../repositories/in-memory/carsRepositoryInMemory';
import { SpecsRepositoryInMemory } from '../../repositories/in-memory/SpecRepositoryInMemory';
import { CreateCarSpecificationUseCase } from './createCarSpecificationUseCase';

let createCarSpecUseCase: CreateCarSpecificationUseCase;
let carsRepoInMemory: CarsRepoInMemory;
let specsRepoInMemory: SpecsRepositoryInMemory;
describe('create car specification', () => {
  beforeEach(() => {
    carsRepoInMemory = new CarsRepoInMemory();
    specsRepoInMemory = new SpecsRepositoryInMemory();
    createCarSpecUseCase = new CreateCarSpecificationUseCase(
      carsRepoInMemory,
      specsRepoInMemory,
    );
  });
  it('Should be able to add a new specification to the car', async () => {
    const car = await carsRepoInMemory.createCar({
      name: 'corse',
      description: 'carro bão',
      daily_rate: 30,
      license_plate: 'abc1234',
      fine_amount: 12,
      brand: 'Chevrolet',
      category_id: '12',
    });

    const specification = await specsRepoInMemory.create({
      description: 'teste',
      name: 'test',
    });

    const specifications_id = [specification.id];
    const specificationsCars = await createCarSpecUseCase.execute({
      car_id: car.id,
      specifications_id,
    });
    expect(specificationsCars).toHaveProperty('specifications');
    expect(specificationsCars.specifications.length).toBe(1);
  });
  it('Should not be able to add a new specification to a car that doesnt exist', async () => {
    expect(async () => {
      const car_id = '1234';
      const specifications_id = ['5432'];
      await createCarSpecUseCase.execute({ car_id, specifications_id });
    }).rejects.toBeInstanceOf(AppError);
  });
});
