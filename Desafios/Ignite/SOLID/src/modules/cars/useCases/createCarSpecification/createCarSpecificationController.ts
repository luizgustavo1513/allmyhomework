import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { CreateCarSpecificationUseCase } from './createCarSpecificationUseCase';

class CreateCarSpecificationController {
  async handle(req: Request, res: Response): Promise<Response> {
    const { id } = req.params;
    const { specifications_id } = req.body;

    const createCarSpecUseCase = container.resolve(
      CreateCarSpecificationUseCase,
    );

    const cars = await createCarSpecUseCase.execute({
      car_id: id,
      specifications_id,
    });

    return res.json(cars).status(200);
  }
}

export { CreateCarSpecificationController };
