import { AppError } from '../../../../shared/errors/AppErros';
import { CarsRepoInMemory } from '../../repositories/in-memory/carsRepositoryInMemory';
import { CreateCarUseCase } from './createCarUseCase';

let createCarUseCase: CreateCarUseCase;
let carsRepo: CarsRepoInMemory;

describe('Create car', () => {
  beforeEach(() => {
    carsRepo = new CarsRepoInMemory();
    createCarUseCase = new CreateCarUseCase(carsRepo);
  });

  it('should be able to create a new car', async () => {
    await createCarUseCase.execute({
      name: 'corse',
      description: 'carro bão',
      daily_rate: 30,
      license_plate: 'abc1234',
      fine_amount: 12,
      brand: 'Chevrolet',
      category_id: '12',
    });
  });
  it('should not be able to register two cars with the same license plate', () => {
    expect(async () => {
      await createCarUseCase.execute({
        name: 'corsa',
        description: 'carro bão',
        daily_rate: 30,
        license_plate: 'abc1234',
        fine_amount: 12,
        brand: 'Chevrolet',
        category_id: '12',
      });
      await createCarUseCase.execute({
        name: 'corse',
        description: 'carro bão',
        daily_rate: 30,
        license_plate: 'abc1234',
        fine_amount: 12,
        brand: 'Chevrolet',
        category_id: '12',
      });
    }).rejects.toBeInstanceOf(AppError);
  });
  it('should be able to any car to be available from creation', async () => {
    const car = await createCarUseCase.execute({
      name: 'Car Available',
      description: 'carro bão',
      daily_rate: 30,
      license_plate: 'abc1234',
      fine_amount: 12,
      brand: 'Chevrolet',
      category_id: '12',
    });
    expect(car.available).toBe(true);
  });
});
