import { inject, injectable } from 'tsyringe';

import { AppError } from '../../../../shared/errors/AppErros';
import { Car } from '../../infra/typeorm/entities/cars';
import { ICarsRepositories } from '../../repositories/ICarsRepositories';

interface IRequest {
  name: string;
  description: string;
  daily_rate: number;
  license_plate: string;
  fine_amount: number;
  brand: string;
  category_id: string;
}

@injectable()
class CreateCarUseCase {
  constructor(
    @inject('PostGresCarsRepo')
    private carRepository: ICarsRepositories,
  ) {}

  async execute({
    name,
    description,
    daily_rate,
    license_plate,
    fine_amount,
    brand,
    category_id,
  }: IRequest): Promise<Car> {
    const carAlreadyExists = await this.carRepository.getCarByPlate(
      license_plate,
    );

    if (carAlreadyExists) {
      throw new AppError('Car already exists', 400);
    }

    const car = await this.carRepository.createCar({
      name,
      description,
      daily_rate,
      license_plate,
      fine_amount,
      brand,
      category_id,
    });

    return car;
  }
}

export { CreateCarUseCase };
