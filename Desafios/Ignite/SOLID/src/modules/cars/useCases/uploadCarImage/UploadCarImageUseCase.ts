import { inject, injectable } from 'tsyringe';

import { ICarsImagesRepositories } from '../../repositories/ICarsImagesRepositories';

interface IRequest {
  car_id: string;
  images_name: string[];
}

@injectable()
class UploadCarImageUseCase {
  constructor(
    @inject('CarsImagesRepository')
    private carsImageRepo: ICarsImagesRepositories,
  ) {}
  async execute({ car_id, images_name }: IRequest): Promise<void> {
    images_name.map(async image => this.carsImageRepo.create(car_id, image));
  }
}

export { UploadCarImageUseCase };
