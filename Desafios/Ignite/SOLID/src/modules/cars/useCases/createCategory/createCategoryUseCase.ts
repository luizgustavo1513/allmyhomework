import { inject, injectable } from 'tsyringe';

import { AppError } from '../../../../shared/errors/AppErros';
import { ICategoriesRepositories } from '../../repositories/ICategoriesRepositories';

// toda essa seção serve pra desmembrar as responsabilidades das rotas e seguir o primeiro principio de solid que é: uma função deve ter apenas uma tarefa a ser executada. Logo, estamos fazenos outras funções dentro de um objeto para realizar a tarefa da rota sem atribuir mais que uma responsabilidade pra ela

interface IRequest {
  name: string;
  description: string;
}

/** O que temos que fazer:
 * [] - Definir nosso retorno
 * [] - Alterar o retorno do erro
 * [] - Acessar o repositório
 */

@injectable()
class CreateCategoryUseCase {
  constructor(
    @inject('PostgressCategoriesRepositories')
    private categoriesRepository: ICategoriesRepositories,
  ) {}

  async execute({ name, description }: IRequest): Promise<void> {
    const category = await this.categoriesRepository.findByName(name);

    if (category) {
      throw new AppError('Category already exists!'); // erro a ser lançado pra quem faz a requisição
    }

    await this.categoriesRepository.create({ name, description });
  }
}

export { CreateCategoryUseCase };
