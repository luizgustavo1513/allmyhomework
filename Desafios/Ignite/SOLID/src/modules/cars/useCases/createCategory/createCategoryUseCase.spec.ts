import { AppError } from '../../../../shared/errors/AppErros';
import { CategoriesRepositoryInMemory } from '../../repositories/in-memory/categoriesRepositoryInMemory';
import { CreateCategoryUseCase } from './createCategoryUseCase';

let createCategory: CreateCategoryUseCase;

let createCategoriesRepository: CategoriesRepositoryInMemory;

describe('Create Category', () => {
  beforeEach(() => {
    createCategoriesRepository = new CategoriesRepositoryInMemory();
    createCategory = new CreateCategoryUseCase(createCategoriesRepository);
  });
  it('Should be able to create a new category', async () => {
    const category = {
      name: 'test',
      description: 'test category',
    };
    await createCategory.execute(category);

    const categoryCreated = await createCategoriesRepository.findByName(
      category.name,
    );

    expect(categoryCreated).toHaveProperty('id');
  });
  it('Should not be able to create a new category with same name', async () => {
    expect(async () => {
      const category = {
        name: 'test',
        description: 'test category',
      };
      await createCategory.execute(category);

      await createCategory.execute(category);
    }).rejects.toBeInstanceOf(AppError);
  });
});
