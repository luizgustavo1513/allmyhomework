import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { CreateCategoryUseCase } from './createCategoryUseCase';

class CreateCategoryController {
  async handle(req: Request, res: Response): Promise<Response> {
    const { name, description } = req.body;

    const createCategoryUseCase = container.resolve(CreateCategoryUseCase);

    // try {
    await createCategoryUseCase.execute({ name, description });
    return res.status(201).send();
    // } catch (e) {
    //  return res.status(400).json({ message: (e as Error).message });
    // }
  }
}

export { CreateCategoryController };
