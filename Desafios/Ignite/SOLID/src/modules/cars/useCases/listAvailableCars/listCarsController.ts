import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { ListCarsUseCase } from './listCarsUseCase';

class ListCarsControllers {
  async handle(req: Request, res: Response): Promise<Response> {
    const { name, category_id, brand } = req.query;
    const listCarsUseCase = container.resolve(ListCarsUseCase);

    const lista = await listCarsUseCase.execute({
      name: name as string,
      category_id: category_id as string,
      brand: brand as string,
    });

    return res.status(200).json(lista);
  }
}

export { ListCarsControllers };
