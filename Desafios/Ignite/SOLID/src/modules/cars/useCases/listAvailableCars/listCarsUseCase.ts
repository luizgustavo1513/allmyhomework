import { inject, injectable } from 'tsyringe';

import { Car } from '../../infra/typeorm/entities/cars';
import { ICarsRepositories } from '../../repositories/ICarsRepositories';

interface IRequest {
  category_id?: string;
  brand?: string;
  name?: string;
}

@injectable()
class ListCarsUseCase {
  constructor(
    @inject('PostGresCarsRepo')
    private carsRepo: ICarsRepositories,
  ) {}
  async execute({
    category_id,
    brand,
    name,
  }: IRequest): Promise<Car[] | undefined> {
    const listaCarros = await this.carsRepo.listCars(name, category_id, brand);
    return listaCarros;
  }
}

export { ListCarsUseCase };
