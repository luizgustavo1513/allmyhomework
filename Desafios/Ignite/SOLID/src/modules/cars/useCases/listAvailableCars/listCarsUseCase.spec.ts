import { CarsRepoInMemory } from '../../repositories/in-memory/carsRepositoryInMemory';
import { CreateCarUseCase } from '../createCar/createCarUseCase';
import { ListCarsUseCase } from './listCarsUseCase';

let carsRepo: CarsRepoInMemory;
let listCarsUseCase: ListCarsUseCase;
let createCarsUseCase: CreateCarUseCase;

describe('Cars List', () => {
  beforeEach(() => {
    carsRepo = new CarsRepoInMemory();
    listCarsUseCase = new ListCarsUseCase(carsRepo);
    createCarsUseCase = new CreateCarUseCase(carsRepo);
  });

  it('Should be able to list all cars', async () => {
    const car = await createCarsUseCase.execute({
      name: 'asas',
      description: 'asas',
      daily_rate: 140,
      license_plate: 'abas',
      fine_amount: 20,
      brand: 'asas',
      category_id: 'asas',
    });
    await createCarsUseCase.execute({
      name: 'asas',
      description: 'asas',
      daily_rate: 140,
      license_plate: 'abs',
      fine_amount: 20,
      brand: 'asas',
      category_id: 'asas',
    });
    await createCarsUseCase.execute({
      name: 'asas',
      description: 'asas',
      daily_rate: 140,
      license_plate: 'abaas',
      fine_amount: 20,
      brand: 'asas',
      category_id: 'asas',
    });
    const listaCarros = await listCarsUseCase.execute({});

    expect(listaCarros).toEqual(expect.arrayContaining([car]));
  });

  it('Should be able to list all available cars by name', async () => {
    const car = await createCarsUseCase.execute({
      name: 'asas',
      description: 'asas',
      daily_rate: 140,
      license_plate: 'abaas',
      fine_amount: 20,
      brand: 'asas',
      category_id: 'asas',
    });
    await createCarsUseCase.execute({
      name: 'asass',
      description: 'asaws',
      daily_rate: 140,
      license_plate: 'abadas',
      fine_amount: 20,
      brand: 'asasss',
      category_id: 'asasss',
    });
    const listaCarros = await listCarsUseCase.execute({
      name: 'asass',
    });

    expect(listaCarros).toEqual(expect.arrayContaining([car]));
  });

  it('Should be able to list all available cars by brand', async () => {
    const car = await createCarsUseCase.execute({
      name: 'asas',
      description: 'asas',
      daily_rate: 140,
      license_plate: 'abaas',
      fine_amount: 20,
      brand: 'asas',
      category_id: 'asas',
    });
    await createCarsUseCase.execute({
      name: 'asass',
      description: 'asaws',
      daily_rate: 140,
      license_plate: 'abadas',
      fine_amount: 20,
      brand: 'asasss',
      category_id: 'asasss',
    });
    const listaCarros = await listCarsUseCase.execute({
      brand: 'asas',
    });

    expect(listaCarros).toEqual(expect.arrayContaining([car]));
  });

  it('Should be able to list all available cars by category', async () => {
    const car = await createCarsUseCase.execute({
      name: 'asas',
      description: 'asas',
      daily_rate: 140,
      license_plate: 'abaas',
      fine_amount: 20,
      brand: 'asas',
      category_id: 'asas',
    });
    await createCarsUseCase.execute({
      name: 'asass',
      description: 'asaws',
      daily_rate: 140,
      license_plate: 'abadas',
      fine_amount: 20,
      brand: 'asasss',
      category_id: 'asasss',
    });
    const listaCarros = await listCarsUseCase.execute({
      category_id: 'asasss',
    });

    expect(listaCarros).toEqual(expect.arrayContaining([car]));
  });
});
