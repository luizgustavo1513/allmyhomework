import { parse } from 'csv-parse';
import fs from 'fs';
import { inject, injectable } from 'tsyringe';

import { ICategoriesRepositories } from '../../repositories/ICategoriesRepositories';

interface IImportCategory {
  name: string;
  description: string;
}

@injectable()
class ImportCategoryUseCase {
  constructor(
    @inject('PostgressCategoriesRepositories')
    private categoriesRepository: ICategoriesRepositories,
  ) {}

  loadCategories(file: Express.Multer.File): Promise<Array<IImportCategory>> {
    return new Promise((resolve, reject) => {
      const stream = fs.createReadStream(file.path); // stream só é utilizado porque o arquivo é pequeno, se fosse grande pesaria o processamento. FS = File System é nativo do node e tem funções para manipulação de arquivos

      const categories: IImportCategory[] = [];

      const parseFile = parse(); // por padrão ele entende o delimitador como sendo vírgula mas podemos colocar outros entre chaves dentro dos parẽntese / função que monta uma matriz de acordo com o delimitador e por linha, faz objetos com propriedades separadas por vírgulas e objetos diferentes por linhas

      stream.pipe(parseFile);

      parseFile // função do tipo asíncrona para pegar todo nome e descrição de cada linha
        .on('data', async line => {
          const [name, description] = line;
          categories.push({ name, description });
        })
        .on('end', () => {
          fs.promises.unlink(file.path); // remove o arquivo da aplicação no caminho específicado. Agora quando importado o arquivo é lido e registrado mas não fica mais em pasta alguma
          resolve(categories); // quando a funcionalidade acabar ele termina e manda o file inteiro transfomado em um array de objetos
        })
        .on('error', err => {
          reject(err);
        });
    });
  }

  async execute(file: Express.Multer.File): Promise<void> {
    const categories = await this.loadCategories(file);
    console.log(categories);

    categories.map(async category => {
      const { name, description } = category;

      const existentCategory = await this.categoriesRepository.findByName(name);

      if (!existentCategory) {
        await this.categoriesRepository.create({ name, description });
      }
    });
  }
}

export { ImportCategoryUseCase };
