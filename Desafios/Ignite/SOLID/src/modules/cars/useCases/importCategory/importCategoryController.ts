import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { ImportCategoryUseCase } from './importCategoryUseCase';

class ImportCategoryController {
  async handle(req: Request, res: Response): Promise<Response> {
    const { file } = req;

    const importCategoryUseCase = container.resolve(ImportCategoryUseCase);

    if (!file) {
      return res.json({ Error: "File doesn't exist" });
    }
    await importCategoryUseCase.execute(file);

    return res.status(200).send();
  }
}

export { ImportCategoryController };
