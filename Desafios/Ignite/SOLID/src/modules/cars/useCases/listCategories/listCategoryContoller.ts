import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { ListCategoryUseCase } from './listCategoryUseCase';

class ListCategoryController {
  async handle(req: Request, res: Response): Promise<Response> {
    const listCategoryUseCase = container.resolve(ListCategoryUseCase);
    try {
      const CategoriesList = await listCategoryUseCase.execute();
      return res.status(200).json(CategoriesList);
    } catch (e) {
      return res.status(400).json({ message: (e as Error).message });
    }
  }
}

export { ListCategoryController };
