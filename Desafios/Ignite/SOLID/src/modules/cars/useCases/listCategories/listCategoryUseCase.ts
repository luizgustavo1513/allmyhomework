import { inject, injectable } from 'tsyringe';

import { Category } from '../../infra/typeorm/entities/category';
import { ICategoriesRepositories } from '../../repositories/ICategoriesRepositories';

@injectable()
class ListCategoryUseCase {
  constructor(
    @inject('PostgressCategoriesRepositories')
    private categoriesRepository: ICategoriesRepositories,
  ) {}

  async execute(): Promise<Category[]> {
    const categories = await this.categoriesRepository.list();

    return categories;
  }
}

export { ListCategoryUseCase };
