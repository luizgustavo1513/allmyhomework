import { hash } from 'bcrypt';
import request from 'supertest';
import { Connection } from 'typeorm';
import { v4 as uuidv4 } from 'uuid';

import { app } from '../../../../shared/infra/http/app';
import createConnection from '../../../../shared/infra/typeorm';

let connection: Connection;
describe('List Categories', () => {
  beforeAll(async () => {
    connection = await createConnection();
    await connection.runMigrations();

    const id = uuidv4();
    const password = await hash('admin2', 8);

    await connection.query(
      `INSERT INTO USERS(id,name,username,password, admin, created_at, driver_license ) 
    values('${id}', 'admin', 'adminuser2', '${password}', true, 'now()', 'xxxxxxx')
    `,
    );
  });
  afterAll(async () => {
    await connection.dropDatabase();
    await connection.close();
  });
  it('Should be able to list categories', async () => {
    const responseToken = await request(app)
      .post('/sessions')
      .send({ username: 'adminuser2', password: 'admin2' });

    const { token } = responseToken.body;

    await request(app)
      .post('/categories')
      .send({ name: 'super', description: 'Bate 200 na BR' })
      .set({ Authorization: `Bearer ${token}` });
    await request(app)
      .post('/categories')
      .send({ name: 'esportivo', description: 'carro de gente fina' })
      .set({ Authorization: `Bearer ${token}` });
    await request(app)
      .post('/categories')
      .send({ name: 'compacto', description: 'fraco' })
      .set({ Authorization: `Bearer ${token}` });

    const response = await request(app).get('/categories');

    console.log(response.body);

    expect(response.status).toBe(200);
    expect(response.body.length).toBe(3);
    expect(response.body[0]).toHaveProperty('id');
    expect(response.body[0].name).toEqual('super');
  });
});
