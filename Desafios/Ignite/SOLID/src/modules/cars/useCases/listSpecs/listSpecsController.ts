import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { ListSpecUseCase } from './listSpecsUseCase';

class ListSpecController {
  async handle(req: Request, res: Response): Promise<Response> {
    const listSpecsUseCase = container.resolve(ListSpecUseCase);
    const listSpec = await listSpecsUseCase.execute();

    return res.send(listSpec).status(201);
  }
}

export { ListSpecController };
