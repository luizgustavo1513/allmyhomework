import { inject, injectable } from 'tsyringe';

import { Category } from '../../infra/typeorm/entities/category';
import { ISpecRepositories } from '../../repositories/ISpecRepositories';

@injectable()
class ListSpecUseCase {
  constructor(
    @inject('SpecsRepository')
    private SpecsRepository: ISpecRepositories,
  ) {}

  async execute(): Promise<Category[]> {
    const listSpecification = await this.SpecsRepository.list();

    return listSpecification;
  }
}

export { ListSpecUseCase };
