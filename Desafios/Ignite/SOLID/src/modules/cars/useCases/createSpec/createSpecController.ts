import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { CreateSpecUseCase } from './createSpecUseCase';

class CreateSpecController {
  async handle(req: Request, res: Response): Promise<Response> {
    const { name, description } = req.body;

    const createSpecUseCase = container.resolve(CreateSpecUseCase);

    // try {
    await createSpecUseCase.execute({ name, description });
    return res.status(201).send();
    // } catch (e) {
    //  return res.status(400).json({ message: (e as Error).message });
    // }
  }
}

export { CreateSpecController };
