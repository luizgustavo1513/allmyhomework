import { inject, injectable } from 'tsyringe';

import { AppError } from '../../../../shared/errors/AppErros';
import { ISpecRepositories } from '../../repositories/ISpecRepositories';

interface IRequest {
  name: string;
  description: string;
}

@injectable()
class CreateSpecUseCase {
  constructor(
    @inject('SpecsRepository')
    private SpecsRepository: ISpecRepositories,
  ) {}

  async execute({ name, description }: IRequest): Promise<void> {
    const specification = await this.SpecsRepository.findByName(name);

    if (specification) {
      throw new AppError('Specification already exists');
    }

    this.SpecsRepository.create({ name, description });
  }
}

export { CreateSpecUseCase };
