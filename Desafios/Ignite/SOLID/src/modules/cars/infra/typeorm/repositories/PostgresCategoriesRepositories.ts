import { getRepository, Repository } from 'typeorm';

import {
  ICategoriesRepositories,
  ICreateCategoryDTO,
} from '../../../repositories/ICategoriesRepositories';
import { Category } from '../entities/category';

class PostgressCategoriesRepositories implements ICategoriesRepositories {
  private repository: Repository<Category>;

  constructor() {
    this.repository = getRepository(Category);
  }

  async create({ description, name }: ICreateCategoryDTO): Promise<void> {
    const category = this.repository.create({
      description,
      name,
    });
    await this.repository.save(category);
  }
  async findByName(name: string): Promise<Category | undefined> {
    const category = await this.repository.findOne({ name });
    return category;
  }
  async list(): Promise<Category[]> {
    const category = await this.repository.find();
    return category;
  }
}

export { PostgressCategoriesRepositories };
