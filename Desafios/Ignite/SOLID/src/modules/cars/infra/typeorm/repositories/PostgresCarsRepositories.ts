import { getRepository, Repository } from 'typeorm';

import { ICreateCarDTO } from '../../../dtos/ICreateCarDTO';
import { ICarsRepositories } from '../../../repositories/ICarsRepositories';
import { Car } from '../entities/cars';

class PostgresCarsRepositories implements ICarsRepositories {
  private repository: Repository<Car>;

  constructor() {
    this.repository = getRepository(Car);
  }

  async createCar({
    id,
    name,
    description,
    daily_rate,
    license_plate,
    fine_amount,
    brand,
    category_id,
    specifications,
  }: ICreateCarDTO): Promise<Car> {
    const car = this.repository.create({
      name,
      description,
      daily_rate,
      license_plate,
      fine_amount,
      brand,
      category_id,
      specifications,
      id,
    });
    await this.repository.save(car);
    return car;
  }
  async listCars(
    name?: string,
    category_id?: string,
    brand?: string,
  ): Promise<Car[] | undefined> {
    const carQuerry = this.repository
      .createQueryBuilder('c')
      .where('available = :available', { available: true });

    if (brand) {
      carQuerry.andWhere('c.brand = :brand', { brand });
    }
    if (name) {
      carQuerry.andWhere('c.name = :name', { name });
    }
    if (category_id) {
      carQuerry.andWhere('c.category_id = :category_id', { category_id });
    }
    const cars = await carQuerry.getMany();

    return cars;
  }
  async getCarByPlate(plate: string): Promise<Car | undefined> {
    const car = this.repository.findOne({ license_plate: plate });
    return car;
  }

  async findById(id: string): Promise<Car | undefined> {
    const car = await this.repository.findOne({ id });
    return car;
  }

  //   async createCarSpec(): Promise<void> {
  //     throw new Error('Method not implemented.');
  //   }
  //   async createCarImage(): Promise<void> {
  //     throw new Error('Method not implemented.');
  //   }
  //   async rentCar(): Promise<void> {
  //     throw new Error('Method not implemented.');
  //   }

  async updateAvailable(id: string, available: boolean): Promise<void> {
    await this.repository
      .createQueryBuilder()
      .update()
      .set({ available })
      .where('id = :id')
      .setParameters({ id })
      .execute();
  }
}

export { PostgresCarsRepositories };
