import { Column, CreateDateColumn, Entity, PrimaryColumn } from 'typeorm';
import { v4 as uuidv4 } from 'uuid';

@Entity('specifications')
class Specs {
  @PrimaryColumn()
  id?: string;

  @Column()
  name: string;

  @Column()
  description: string;

  @CreateDateColumn()
  created_at: Date;

  constructor() {
    // função realizada quando novo objeto é criado / O this é usado pra pegar a propriedade do objeto criado em si
    if (!this.id) {
      this.id = uuidv4();
    }
  }
}

export { Specs };
