import { getRepository, Repository } from 'typeorm';

import {
  ICreateSpecDTO,
  ISpecRepositories,
} from '../../../repositories/ISpecRepositories';
import { Specs } from '../entities/specs';

class PostgresSpecsRepositories implements ISpecRepositories {
  private repository: Repository<Specs>;

  constructor() {
    this.repository = getRepository(Specs);
  }

  async create({ name, description }: ICreateSpecDTO): Promise<Specs> {
    const specification = this.repository.create({
      description,
      name,
    });

    await this.repository.save(specification);
    return specification;
  }
  async findByName(name: string): Promise<Specs | undefined> {
    const specification = await this.repository.findOne({ name });
    return specification;
  }
  async list(): Promise<Specs[]> {
    const specification = await this.repository.find();
    return specification;
  }
  async findByIds(ids: (string | undefined)[]): Promise<Specs[]> {
    const specifications = await this.repository.findByIds(ids);
    return specifications;
  }
}

export { PostgresSpecsRepositories };
