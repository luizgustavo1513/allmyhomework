import { ICreateCarImageDTO } from '../dtos/ICreateCarImageDTO';

interface ICarsImagesRepositories {
  create(car_id: string, image_name: string): Promise<ICreateCarImageDTO>;
}

export { ICarsImagesRepositories };
