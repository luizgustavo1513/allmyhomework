import { ICreateCarDTO } from '../dtos/ICreateCarDTO';
import { Car } from '../infra/typeorm/entities/cars';

interface ICarsRepositories {
  createCar(data: ICreateCarDTO): Promise<Car>;
  listCars(
    brand?: string,
    name?: string,
    category_id?: string,
  ): Promise<Car[] | undefined>;
  getCarByPlate(plate: string): Promise<Car | undefined>;
  findById(id: string | undefined): Promise<Car | undefined>;
  updateAvailable(id: string, available: boolean): Promise<void>;
  // createCarSpec(): Promise<void>;
  // createCarImage(): Promise<void>;
  // rentCar(): Promise<void>;
}

export { ICarsRepositories };
