import { Specs } from '../infra/typeorm/entities/specs';

interface ICreateSpecDTO {
  name: string;
  description: string;
}

interface ISpecRepositories {
  create({ name, description }: ICreateSpecDTO): Promise<Specs> | void;
  findByName(name: string): Promise<Specs | undefined>;
  list(): Promise<Specs[]>;
  findByIds(ids: (undefined | string)[]): Promise<Specs[]>;
}

export { ISpecRepositories, ICreateSpecDTO };
