import { Category } from '../../infra/typeorm/entities/category';
import {
  ICategoriesRepositories,
  ICreateCategoryDTO,
} from '../ICategoriesRepositories';

// modelo singleton usa apenas uma instância como classe para todo o projeto

class CategoriesRepositories implements ICategoriesRepositories {
  private categories: Category[];

  private static instance: CategoriesRepositories; // Método capaz de instanciar o repositório

  private constructor() {
    this.categories = [];
  }

  public static getInstance(): CategoriesRepositories {
    // caso haja uma instância já criada repassa ela, caso não tenha crie uma
    if (!CategoriesRepositories.instance) {
      CategoriesRepositories.instance = new CategoriesRepositories(); // o constructor dessa classe passa a ser apenas executável dentro dela mesma por ser privado
    }
    return CategoriesRepositories.instance;
  }

  async create({ name, description }: ICreateCategoryDTO): Promise<void> {
    // const category: Category = { Dessa maneira as características da classe são atribuidas ao objeto criado, diferente de criar um novo objeto com as características da classe. Ex: o constructor não roda quando feito dessa maneira
    //   name,
    //   description,
    //   created_at: new Date(),
    // };

    const category = new Category(); // dessa maneira nós criamos um objeto do 0 com as características da classe Category fazendo com que o constructor seja rodado

    Object.assign(category, {
      // assim nós passamos os valores pro objeto
      name,
      description,
      created_at: new Date(),
    });

    this.categories.push(category);
  }
  async list(): Promise<Category[]> {
    return this.categories;
  }
  async findByName(name: string): Promise<Category | undefined> {
    const category = this.categories.find(category => category.name === name);
    return category;
  }
}

export { CategoriesRepositories };
