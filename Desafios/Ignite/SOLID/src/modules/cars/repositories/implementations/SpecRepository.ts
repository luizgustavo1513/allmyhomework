import { Specs } from '../../infra/typeorm/entities/specs';
import { ICreateSpecDTO, ISpecRepositories } from '../ISpecRepositories';

class SpecsRepository implements ISpecRepositories {
  private specs: Specs[];

  private static instance: SpecsRepository;

  constructor() {
    this.specs = [];
  }
  findByIds(ids: (string | undefined)[]): Promise<Specs[]> {
    throw new Error('Method not implemented.');
  }

  public static getStatic(): SpecsRepository {
    if (!SpecsRepository.instance) {
      SpecsRepository.instance = new SpecsRepository();
    }
    return SpecsRepository.instance;
  }

  create({ name, description }: ICreateSpecDTO): void {
    const specification = new Specs();

    Object.assign(specification, {
      name,
      description,
      created_at: new Date(),
    });

    this.specs.push(specification);
  }

  async findByName(name: string): Promise<Specs | undefined> {
    const specification = this.specs.find(
      specification => specification.name === name,
    );
    return specification;
  }

  async list(): Promise<Specs[]> {
    return this.specs;
  }
}

export { SpecsRepository };
