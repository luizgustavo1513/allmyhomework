import { ICreateCarDTO } from '../../dtos/ICreateCarDTO';
import { Car } from '../../infra/typeorm/entities/cars';
import { ICarsRepositories } from '../ICarsRepositories';

class CarsRepoInMemory implements ICarsRepositories {
  cars: Car[] = [];
  async createCar({
    name,
    description,
    daily_rate,
    license_plate,
    fine_amount,
    brand,
    category_id,
    specifications,
  }: ICreateCarDTO): Promise<Car> {
    const car = new Car();

    Object.assign(car, {
      name,
      description,
      daily_rate,
      license_plate,
      fine_amount,
      brand,
      category_id,
      specifications,
    });

    this.cars.push(car);
    return car;
  }
  async listCars(
    name?: string,
    category_id?: string,
    brand?: string,
  ): Promise<Car[] | undefined> {
    const all = this.cars.filter(car => {
      if (
        car.available === true ||
        (brand && car.brand === brand) ||
        (category_id && car.category_id === category_id) ||
        (name && car.name === name)
      ) {
        return car;
      }

      return null;
    });

    return all;
  }
  async getCarByPlate(plate: string): Promise<Car | undefined> {
    const car = this.cars.find(car => car.license_plate === plate);
    return car;
  }
  async findById(id: string): Promise<Car | undefined> {
    const car = this.cars.find(car => car.id === id);
    return car;
  }
  async updateAvailable(id: string, available: boolean): Promise<void> {
    const findIndex = this.cars.findIndex(car => car.id === id);
    this.cars[findIndex].available = available;
  }
}

export { CarsRepoInMemory };
