import { Specs } from '../../infra/typeorm/entities/specs';
import { ICreateSpecDTO, ISpecRepositories } from '../ISpecRepositories';

class SpecsRepositoryInMemory implements ISpecRepositories {
  specs: Specs[] = [];
  async create({ name, description }: ICreateSpecDTO): Promise<Specs> {
    const specification = new Specs();

    Object.assign(specification, {
      name,
      description,
      created_at: new Date(),
    });

    this.specs.push(specification);
    return specification;
  }

  async findByName(name: string): Promise<Specs | undefined> {
    const specification = this.specs.find(
      specification => specification.name === name,
    );
    return specification;
  }

  async list(): Promise<Specs[]> {
    return this.specs;
  }
  async findByIds(ids: (undefined | string)[]): Promise<Specs[]> {
    const specifications = this.specs.filter(specification =>
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      ids.includes(specification.id!),
    );

    return specifications;
  }
}

export { SpecsRepositoryInMemory };
