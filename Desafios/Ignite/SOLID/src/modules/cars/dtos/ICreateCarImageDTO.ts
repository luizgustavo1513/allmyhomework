class ICreateCarImageDTO {
  id: string;
  car_id: string;
  image_name: string;
  created_at: Date;
}

export { ICreateCarImageDTO };
