import { Specs } from '../infra/typeorm/entities/specs';

class ICreateCarDTO {
  id?: string;
  name: string;
  description: string;
  daily_rate: number;
  license_plate: string;
  fine_amount: number;
  brand: string;
  category_id: string;
  specifications?: Specs[];
}

export { ICreateCarDTO };
