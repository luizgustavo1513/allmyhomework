# Cadastro de Carro

**RF** => Requisitos Funcionais

[x] = Deve ser possível cadastrar um carro  
[x] = Deve ser possível listar todas as categorias

**RNF** => Requisitos não funcionais

---

**RN** => Regras de negócio

[x] = Não deve ser possível cadastrar um carro com uma placa já existente.
[x] = O carro deve ser cadastrado com disponibilidade por padrão.
[x] = \* Não deve ser possível qualquer outro usuário que não seja administrador cadastrar um carro.

# Listagem de Carros

**RF**

[x] = Deve ser possível listar todos os carros disponíveis
[x] = Deve ser possível listar todos os carros disponíveis pelo nome da marca
[x] = Deve ser possível listar todos os carros disponíveis pelo nome da categoria
[x] = Deve ser possível listar todos os carros disponíveis pelo nome do carro

**RN**

[x] = O usuário não precisa estar logado no sistema

# Cadastro de Especificação no carro

**RF**

[] = Deve ser possível cadastrar uma especialidade para um carro

**RN**

[] = Não deve ser possível cadastrar uma especificação para um carro não cadastrado
[] = Não deve ser possível cadastrar a mesma especificação duas vezes para o mesmo carro
[] = Não deve ser possível qualquer outro usuário que não seja administrador cadastrar uma especificação.

# Cadastro de Imagens do carro

**RF**

[] = Deve ser possível cadastrar a imagem do carro
[] = Deve ser possível listar todos os carros

**RNF**

[] = Utilizar o multer para upload de arquivos

**RN**

[] = O usuário deve poder cadastrar mais de uma imagem para o mesmo carro
[] = Não deve ser possível qualquer outro usuário que não seja administrador cadastrar um carro.

# Aluguel de carro

**RF**

[] = Deve ser possível cadastrar um aluguel

**RN**

[] = O aluguel deve ter duração mínima de 24 horas
[] = Não deve ser possível cadastrar um novo aluguel caso já exista um aberto para o mesmo usuário
[] = Não deve ser possível cadastrar um novo aluguel caso já exista um aberto para o mesmo carro
[] = Carros alugados devem estar indisponíveis

# Devolução de carro

**RF**

[] = Deve ser possível realizar a devolução do carro

**RN**

[] = Se o carro for devolvido em menos de 24 horas, deve ser cobrado diária completa.
[] = Ao realizar a devolução, o carro deve ser liberado para outro aluguel.
[] = Ao realizar a devolução, o usuário deve ser liberado para outro aluguel.
[] = Ao realizar a devolução, deve ser calculado o total do aluguel.
[] = Deve ser cobrado multa caso o horário previsto de entrega seja violado.
[] = Caso haja multa, ela deverá ser somada ao aluguel.
