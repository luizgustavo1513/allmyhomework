/**
 * name - string
 * duration - number
 * instruction - string
 */

interface Course {
    name:string, 
    duration?: number, 
    instruction:string
}

class CreateCourseService { 

    execute({name, duration = 8, instruction}: Course){ //caso nosso valor opcional não receba nada a duração é um valor padrão (...=8)
        console.log(name,duration,instruction)
    }
 }

 export default new CreateCourseService() //se eu der um new na função ele não precisa ser instanciado, ele é criado como novo onde importado