import { Request, Response } from "express";
import CreateCourseService from "./CreateCourseService";

export function createCourse(req:Request,res:Response){

    const service = { 
        name: "Node",
        duration: 2,
        instruction: "Luiz"
    }

    CreateCourseService.execute(service)
    return res.send()
}