import express from "express" //equivalente ao const ... = require(...) do JS convencional
import { createCourse } from "./routes"

const app = express()

app.get("/", createCourse);

app.listen(3333)