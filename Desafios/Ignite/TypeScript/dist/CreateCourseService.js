"use strict";
/**
 * name - string
 * duration - number
 * instruction - string
 */
Object.defineProperty(exports, "__esModule", { value: true });
class CreateCourseService {
    execute({ name, duration, instruction }) {
        console.log(name, duration, instruction);
    }
}
exports.default = new CreateCourseService(); //se eu der um new na função ele não precisa ser instanciado, ele é criado como novo onde importado
