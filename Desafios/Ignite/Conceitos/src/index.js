const express = require('express');
const cors = require('cors');
const { v4: uuidv4 } = require('uuid');

const app = express();

app.use(cors());
app.use(express.json());

const users = [];

function checksExistsUserAccount(request, response, next) {
  const { username } = request.headers

  const user = users.find((users => users.username === username))

  request.user = user

  if (!user){
    return response.status(404).send({error: "Username doesn't exist"})
  }

  return next()
}

function checksExistingTodo(request, response, next){
  const { id } = request.params
  const { user } = request

  const existingTodo = user.todos.find((todos => todos.id === id))

  request.existingTodo = existingTodo

  if (!existingTodo){
    return response.status(404).send({error: "Todo doesn't exist"})  
  }

  return next()

}

app.post('/users', (request, response) => {
  const { name, username } = request.body

  const existentUser = users.some((users => users.username === username))

  if (existentUser) {
    return response.status(400).send({error: "Username unavailable"})
  }

  else {
  const newUser = {
    id: uuidv4(),
    name,
    username,
    todos: []
  }
  
  users.push(newUser)

  return response.status(201).json(newUser)
}
  
});

app.get('/todos', checksExistsUserAccount, (request, response) => {
  const { user } = request

  return response.status(201).send(user.todos)
});

app.post('/todos', checksExistsUserAccount, (request, response) => {
  const { title, deadline } = request.body
  const { user } = request

  newTodo = {
    id: uuidv4(),
    title,
    done: false,
    deadline: new Date(deadline),
    created_at: new Date
  }

  user.todos.push(newTodo)

  return response.status(201).send(newTodo)

});

app.put('/todos/:id', checksExistsUserAccount, checksExistingTodo, (request, response) => {
  const { title, deadline } = request.body
  const { existingTodo } = request  

  existingTodo.title = title
  existingTodo.deadline = deadline

  return response.status(201).send(existingTodo)
});

app.patch('/todos/:id/done', checksExistsUserAccount, checksExistingTodo,(request, response) => {
  const { existingTodo } = request 

  existingTodo.done = true
  return response.status(201).send(existingTodo)
});

app.delete('/todos/:id', checksExistsUserAccount, checksExistingTodo, (request, response) => {
  const{existingTodo} = request
  const{user} = request

  user.todos.splice(existingTodo,1)

  return response.status(204).send()
});

module.exports = app;