//               T = significa que ele aceita qualquer tipo mas ele só aceita o primeiro tipo que recebe
//                  entends tipo1 Union tipo2 => significa que podem ser do tipo 1 ou 2  e se tiver um igual no fim ele é por 
//                                               padrão esse se nenhum for definido
function useState<T extends string | number = string>(){
    let state: T;

    function get(){
        return state
    }
    function set(newValue:T){
        state = newValue
    }

    return {get,set};
}

let newState = useState()
newState.get();
newState.set(121);
newState.set("Luiz");
newState.set(121)