function showMessage(message:string):void{//funções do tipo void são aquelas que não retornam nada. OS dois pontos do lado dos parametros me dizem qual o tipo de retorno dessa função. Inferencia explicita
    return message
}



/*
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! = \
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! = = \
!!!!!!!!!!!!Importante!!!!!!!!!!!!!!! = = =>  funções do tipo void são aquelas que não retornam nada
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! = = /
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! = /
*/