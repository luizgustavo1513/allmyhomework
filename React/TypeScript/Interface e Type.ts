//tanto tipo quanto interface definem tipagem

//type é mais simples e primitivo

type TUser = {
    id: number,
    name: string
}

type TPayment = {
    method: string
}

type TAccount = TUser & TPayment

//---------------------------       Mesmos resultados formas diferentes       ------------------------------------- 

interface IUser {
    id: number,
    name: string
}

interface IPayment {
    method: string
}

interface IAccount extends IUser, IPayment{

}
