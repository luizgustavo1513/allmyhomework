function showTicket(user:string | null, ticket:number){
    console.log(`Olá ${user  ?? "Usuário Padrão"}. Seu número de bilhete é ${ticket}`) //duas interrogações que fazem o trabalho de um operador ternário que compara se o valor de user é 0 ou nulo e se for ele imprime usuário padrão
}

showTicket('Luiz', 232 )