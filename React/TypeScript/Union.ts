function printUserID(id: number | string | boolean):void{ // função que não retorna nada e passa como parametro qualquer numero, string ou boolean por meio de Union => |
    console.log(`O id do usuário é: ${id}`)
}

printUserID(11)
printUserID('amanhã')
printUserID(true)