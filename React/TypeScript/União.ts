type user = {
    id: number;
    name: string
}

type Char = {
    level: number;
    nickname: string
}

type PlayerInfo = user & Char; //& faz a união de tipos

let info: PlayerInfo = { 
    id: 8,
    name: 'Claudio',
    level: 80,
    nickname: 'Claudinho'
}