type UserResponse = {
    id: number,
    name: string,
    avatar?: string //quando colocada uma interrogação o tipo se torna opcional no objeto
}

let user = {} as UserResponse; //faz com que esse objeto seja adequado ao tipo setado
user.avatar

type Point = {
    x: number,
    y: number
}

function printCord(points:Point){ //insere o tipo no objeto parametro que recebe os mesmos quando a função é chamada
    console.log(`O eixo x é: ${points.x}`)
    console.log(`O eixo y é: ${points.y}`)
}

printCord({ x:100, y:40})//como points é um objeto para passar os parametros para dentro dele precisamos passar entre chaves