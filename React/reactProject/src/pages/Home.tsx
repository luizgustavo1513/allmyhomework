import { useEffect, useState } from "react"
import { Card, Props } from "../components/card"

interface Profile {
  name: string,
  avatar_url: string
}

interface User{
  name: string,
  avatar:string
}

function App() {

//uma variável comum não consegue refletir sua alteração na interface. Pra isso usamos variável estado
const [studentName, setStudentName] = useState('') //a primeira posição do vetor é o nome do estado e a segunda é a função que atualiza o estado e o que for escrito dentro de useState() é o valor inicial
const [students, setStudents] = useState<Props[]>([]) //para ele aplicar os tipos em todo elemento do vetor precisa ser seguido de []
const [user,  setUser] =useState<User>({} as User)

function handleSetStudent(){
  const newStudent = { // cria um objeto  pois precisamos de 2 valores para registro de estudantes, nome e tempo
    name: studentName, // o nome é pego com o que está escrito na caixa de texto
    time: new Date().toLocaleTimeString('pt-br', { //o tempo é pego por essa função diferenciada própria daqui
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit'
     })
  };

  setStudents(prevState =>  [...prevState, newStudent]) //aqui nós recriamos o vetor antigo com um vetor novo que: possui o vetor antigo ( representado por "...prevState" ) que carrega e despeja os valores do vetor antigo no novo vetor e adiciona nessa lista o objeto do novo estudante
}

useEffect(()=>{ //para criar um useEffect assíncrono precisamos criar uma função assíncrona e chamar ela dentro dele pois ele não aceitar async em sua escrita naturalmente
  const url = "https://api.github.com/users/Nanaya-L"
  fetch(url)
  .then(response => response.json())
  .then((data:Profile) => {
    setUser({
      name: data.name,
      avatar: data.avatar_url
    })
  })
},[]) //useEffect é executado automaticamente, ou seja, ele não precisa ser chamado. Quando o array é vazio ele é executado uma única vez, caso um estado seja colocado nessa lista (array de dependencias) ele será chamado toda vez que o estado for atualizado

return (
    <div className="flex items-center flex-col">
      <header className="mt-[84px] mx-0 mb-6 w-1/2 flex justify-between items-center">
        <h1>Lista de Presença</h1>
        <div className="flex items-center">
          <strong>{user.name}</strong>
          <img src={user.avatar} alt="foto de perfil" 
          className="w-[60px] h-[60px] rounded-full ml-[7px] "/>
        </div>
      </header>
    
    
    <input type="text" 
    placeholder="Digite algo." 
    className="w-1/2 p-6 bg-brand-100 rounded text-black text-base"
    onChange={e => setStudentName(e.target.value)}
    />
    <button disabled={studentName.length === 0} type="button" className="w-1/2 p-6 font-bold bg-purple-800 text-white rounded mt-3 mx-0 mb-20 border-none cursor-pointer duration-200 hover:brightness-90 text-base"
    onClick={handleSetStudent}
    >Enviar</button>
    

    {
      students.map(students => (
      <Card 
        key={students.time} //key serve para identificar cada registro da estrutura como único, faz de cada componente uma diferenciação
        name={students.name} 
        time={students.time}/>)) //map percorre todo o vetor e faz a função atribuida para cada valor em seu vetor, ou seja, é uma estrutura de repetição
      }
    
    </div>
  )
}

export default App
