
export interface Props {
  name: string;
  time: string
}

export function Card(props:Props) {
  return (
    <div className="h-[100px] w-1/2 bg-purple-700 text-white rounded mb-[20px] flex items-center justify-between p-6">
      <strong className="text-lg">{props.name} </strong>
      <small>{props.time}</small>
    </div>
  );
}