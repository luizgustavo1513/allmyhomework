module.exports = {
  content: ["./src/**/*.tsx"],
  theme: {
    fontFamily:{
      'Roboto': ['Roboto', 'sans-serif'],
    },
    extend: {
      colors:{
        brand:{
          100:"#E6E6E6",
        }
      }
    },
  },
  plugins: [],
}
