//getElementById() > traz um element

let exemplo = document.getElementById('Exemplo')

console.log(exemplo)

//getElementsByClassName > traz HTML collection

const comp = document.getElementsByClassName('oi')

console.log(comp)

//getElementsByTagName() > traz HTML collection

const tag1 = document.getElementsByTagName('div')

console.log(tag1)

//querySelector() > traz um element

const element = document.querySelector('#mais')

console.log(element)

//querySelectorAll() > faz uma nodelist, não um HTML colection como byclassname

const elements = document.querySelectorAll('.oi')

console.log(elements)

elements.forEach(el => console.log(el))

//Qual usar? depende do que vc precisa. Descrição do que cada seletor pega nos comentários dos mesmos