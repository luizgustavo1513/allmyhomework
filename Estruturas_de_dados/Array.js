let names = ['Pablo', 'Caio', 'Gabriel']

//names.push('Brenda')
//iterável, aceita função for of

for (let name of names) {
    console.log(name)
}

const Caio = names.find(name => name === 'Pablo') //acha o elemento específico dentro do Array mas pode não ser muito eficiente

console.log(Caio)

names.splice(1,2) //não é muito eficiente mas tira elementos em uma posição específica uma quantidade específica a partir dali

console.log(names)


//Matriz

const students =[['José',23,'MG'],['Brenda',22,'SP'],['Priscila',23,'PR']]

console.log(students[0][0])

//aplicação de Pilha

const stack = []

stack.push('learning')

stack.push('data')

console.log(stack)

const pilha = [1,34,1231,124123,6546,2745,276,545,934]

console.log(pilha)

pilha.push(123) //push adiciona ao topo da pilha

console.log(pilha)

pilha.pop()//pop tira o topo da pilha

console.log(pilha)